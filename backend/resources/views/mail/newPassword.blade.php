<div class="password">
  <p><?= $user->name ?> 様</p>
  <br />
  <p>管理者アカウント情報をお知らせします。</p>
  <p>以下の認証用URLからメールアドレスの認証を完了してください。</p>
  <br />
  <p>認証が完了すると自動的に管理者アカウント一覧へ移動しますので、パスワードの変更をお願いいたします。</p>
  <br />
  <p>・パスワードを作成するURL</p>
  <p><?= $newPasswordUrl ?></p>
  <br>
  <p>・ログインURL</p>
  <p><?= env('FRONTEND_URL') ?>/admin/login</p>
  <br />
  <p>・ログインID（メールアドレス）</p>
  <p><?= $user->email ?></p>
  <br />
  <p>※システムのアカウントとして発行したメールアドレスが本人の所有でなくなった場合、受信可能なメールアドレスでなくなった場合は速やかに変更をしてください。</p>
  <p>※このメールは送信専用のため、返信はできません。</p>
  <p>※このメールにお心当たりがない場合は、お手数ですがメールの破棄をお願いいたします。</p>
</div>
