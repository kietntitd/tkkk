<?php

use App\Utils\MessageCommon;

return [
    'route_not_found' => MessageCommon::MS01_001,
    'page_not_found' => MessageCommon::MS01_002,
    'unauthenticated' => MessageCommon::MS01_003,
    'invalid_token' => MessageCommon::MS01_003,
    'expired_token' => MessageCommon::MS01_005,
    'access_denied' => MessageCommon::MS01_004,
    'something_error' => MessageCommon::MS01_002,
    'product_deletion' => 'product deletion failed',
];
