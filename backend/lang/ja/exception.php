<?php

return [
  'product' => [
     //MS03-045
    'delete_set' => 'セットで登録されている商品を削除できません。',
     //MS03-047
    'delete_priced' => '価格が設定されている商品を削除できません。',
     //MS03-046
    'delete_quantity_greater_zero' => '倉庫に存在している商品を削除できません。',
     //MS03-060
    'delete_set_using' => '台帳で使用されているセットを削除できません。',
     //MS03-061
    'delete_set_have_price' => '単価が登録されているセットを削除できません。',
  ],
  'messages' => [
      'MS03_057' => ':productの商品は:customerで単価が設定されています。'
  ]
];
