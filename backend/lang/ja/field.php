<?php

return [
    //RC_06
    'customers' => [
        'code' => '顧客コード',
        'ctplan_code' => '旧顧客コード',
        'name' => '顧客名',
        'name_kana' => '顧客名_カナ',
        'postal_code' => '郵便番号',
        'address1' => '住所',
        'address2' => '住所 （建物／施設名）',
        'tel' => '電話番号',
        'fax' => 'ファックス',
        'invoice_date' => '締め日',
        'note' => '特記事項 ',
        'email' => 'メールアドレス',
        'status' => 'ステータス',
        'customer' => '顧客',
    ],
    //RC_10
    'customer_stores' => [
        'title_list' => '顧客店舗一覧',
        'title_create' => '顧客店舗新規作成',
        'title_update' => '顧客店舗編集',
        'store_name' => '店舗名',
        'store_code' => '店舗コード',
        'customer_name' => '顧客名',
        'tel' => '電話番号',
        'fax' => 'ファックス',
        'address1' => '住所',
        'address2' => '住所 （建物／施設名）',
        'customer_store' => '顧客店舗',
        'pickup_circle' => '集配サイクル ',
        'business_partner_id' => 'クリーニング工場名',
        'area_id' => '地域',
        'job_id' => '業種',
        'job_type_id' => '業態',
    ],

    //RC_03
    'admins' => [
        'role' => '権限',
        'name' => 'ユーザー名',
        'name_kana' => 'ユーザー名_カナ',
        'email' => 'メールアドレス',
        'status' => 'ステータス',
    ],

    'categories' => [
        'name' => '商品カテゴリ名',
    ],
    //RC_17
    'items' => [
        'code' => 'アイテムコード',
        'name' => 'アイテム名',
        'product_id' => '商品名',
        'size' => 'サイズ',
        'warning_quantity' => '数量',
    ],

    //RC_11
    'customer_departments' => [
        'customer_name' => '顧客名',
        'store_name' => '店舗名',
        'department_name' => '部署名',
        'department_code' => '部署コード'
    ],

    //RC_04
    'users' => [
        'name' => 'ユーザー名',
        'name_kana' => 'ユーザー名_カナ',
        'email' => 'メールアドレス',
        'status' => 'ステータス',
    ],
    //RC_05
    'business_partners' => [
        'type' => 'タイプ',
        'code' => '取引先コード',
        'ctplan_code' => '旧取引先コード',
        'name' => '取引先名',
        'name_kana' => '取引先名_カナ',
        'postal_code' => '郵便番号',
        'address1' => '住所',
        'address2' => '住所（建物／施設名）',
        'tel' => '電話番号',
        'fax' => 'ファックス',
        'email' => 'メールアドレス',
        'status' => 'ステータス',
    ],
    //RC_08
    'products' => [
        'name' => '商品名',
        'category_id' => '商品カテゴリ名',
        'price' => '仕入単価',
        'business_partner_id' => 'メーカー名',
        'label_type' => 'ラベルタイプ',
    ],
    //RC_19
    'sets' => [
        'name' => 'セット名',
        'customer_id' => '顧客名',
        'product' => '商品名',

    ],
// RC_18
    'contracts' => [
        'code' => '基本契約コード',
        'customer_code' => '顧客コード',
        'customer_id' => '顧客名',
        'invoice_date' => '締め日',
        'payment_date' => '支払日',
        'contract_price' => '契約金額（円）',
        'sell_price' => '販売金額（円）',
        'rent_price' => 'レンタル金額（円）',
        'wash_price' => 'クリーニング金額（円）',
        'contract' => '契約',
        'tel' => '電話番号',
        'fax' => 'ファクス',
        'remarks' => '特記事項',
        'contracts_detail' => [
            'basic_contract_id' => '基本契約コード',
            'customer_stores_name' => '店舗名',
            'customer_stores_tel' => '電話番号',
            'customer_stores_address' => '住所',
            'customer_stores_address2' => '住所（建物／施設）',
            'contract_type' => '契約区分',
            'manage_type' => '貸し区分',
            'store_id' => '店舗名',
            'start_date' => '契約開始日',
            'end_date' => '契約終了日',
            'business_partner_id' => 'クリーニング工場コード',
            'tax' => '消費税 (%)',
            'total_price' => '税抜金額',
            'total_price_tax' => '税込金額',
            'remarks' => '特記事項',
            'detail_field' => 'レンタル内訳',
            'pickup_circle' => '集配サイクル',
            'product_name' => 'セット名・商品名',
            'quantity' => '数量',
            'rent_price' => 'レンタル単価（円）',
            'sell_price' => '販売単価（円）',
            'rent_total' => '販売合計金額（円）',
            'sell_total' => 'レンタル合計金額（円）',
            'wash_price' => 'クリーニング単価（円）',
        ]
    ],
    'shelves' => [
        'code' => '棚番',
        'rows' => '段数',
        'columns' => '列数',
    ]
];
