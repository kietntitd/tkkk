<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes for Admins
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    /*
    * API with Auth
    */
    Route::group(['middleware' => ['auth:api']], function () {
        // Update must be use Post method because maybe upload file
    });

    /*
     * API No Auth
    */

    Route::group(['prefix' => 'dashboard'], function () {
        // Route::apiResource('/', 'DashBoardController')->only(['index', 'store', 'show', 'update']);
    });
});
