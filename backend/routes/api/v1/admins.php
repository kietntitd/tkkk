<?php

use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\AuthController;
use App\Http\Controllers\Api\Admin\BasicContractController;
use App\Http\Controllers\Api\Admin\BusinessPartnerController;
use App\Http\Controllers\Api\Admin\CategoryController;
use App\Http\Controllers\Api\Admin\ContractController;
use App\Http\Controllers\Api\Admin\CustomerDepartmentController;
use App\Http\Controllers\Api\Admin\CustomerStoreController;
use App\Http\Controllers\Api\Admin\LedgerController;
use App\Http\Controllers\Api\Admin\ItemController;
use App\Http\Controllers\Api\Admin\MasterController;
use App\Http\Controllers\Api\Admin\ProductController;
use App\Http\Controllers\Api\Admin\SalePriceController;
use App\Http\Controllers\Api\Admin\SetController;
use App\Http\Controllers\Api\Admin\UserController;
use App\Http\Controllers\Api\Admin\SetProductController;
use App\Http\Controllers\Api\Admin\WarehouseShelfController;
use App\Http\Controllers\Api\Customer\CustomerController;
use App\Http\Controllers\Api\Admin\PurchasePriceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes for Admins
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
* API with Auth
*/

Route::group(['middleware' => ['auth:api_admin']], function () {
    /**************************
     ***** CUSTOM ROUTER *****
     **************************/

    // Update must be use Post method because have upload file
    Route::post('/categories/{id}', [CategoryController::class, 'updateWithFile'])->name('categories.updateWithFile');

    // END CUSTOM ROUTER


    /*****************************
     ***** DATA MASTER ROUTER ****
     *****************************/

    // data master for select > option

    Route::controller(MasterController::class)->prefix('master')->group(function () {
        Route::get('/get-customer-options', 'getCustomerOptions');
        Route::get('/get-business-partner-options', 'getBusinessPartnerOptions');
        Route::get('/get-category-options', 'getCategoryOptions');
        Route::get('/get-product-options', 'getProductOptions');
        Route::get('/get-store-attribute-options', 'getStoreAttributeOptions');
        Route::get('/get-customer-store-options', 'getCustomerStoreOptions');
    }); // DATA MASTER ROUTER


    /*************************************
     ******* APIS FOR MASTER ROLE ********
     *************************************/
    Route::group(['middleware' => ['isMaster']], function () {
        Route::apiResource('/admins', AdminController::class);
    });  // APIS FOR MASTER ROLE

    /*****************************
     ******* API RESOURCES ********
     *****************************/
    Route::apiResource('/users', UserController::class)->only(['index', 'store', 'show', 'update']);
    Route::apiResource('/categories', CategoryController::class)->only(['index', 'store', 'show', 'delete']);
    Route::apiResource('/users', UserController::class);
    Route::apiResource('/categories', CategoryController::class);
    Route::apiResource('/business-partners', BusinessPartnerController::class);
    Route::apiResource('/products', ProductController::class);
    Route::apiResource('/customer-departments', CustomerDepartmentController::class);
    Route::apiResource('/sets', SetController::class);
    //Route::apiResource('/ledgers-list', LedgerController::class);
    Route::apiResource('/sale-prices', SalePriceController::class);
    Route::apiResource('/items', ItemController::class);
    Route::apiResource('/customer-stores', CustomerStoreController::class);
    Route::apiResource('/ledgers', LedgerController::class);
    Route::apiResource('/warehouse-shelves', WarehouseShelfController::class);
    Route::apiResource('/customers', CustomerController::class);
    Route::apiResource('/contracts', BasicContractController::class);
    Route::apiResource('/contract-detail', ContractController::class);
    Route::apiResource('/set-products', SetProductController::class);
    Route::apiResource('/purchase-prices', PurchasePriceController::class);


    Route::get('/ledgers/staff/{id}', [LedgerController::class, 'getStaff']);

    // END API RESOURCES

    Route::controller(AuthController::class)->prefix('auth')->group(function () {
        Route::post('resent-password', 'resendConfirmEmail')->name('password.resend');
    });
});


/*
* API No Auth
*/


/**************************
 ***** AUTHENTICATION *****
 **************************/

Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::middleware(['throttle:login'])->group(function () {
        Route::post('login', 'login');
    });
    Route::post('request-password', 'requestPassword')->name('password.request');
    Route::post('reset-password', 'resetPassword')->name('password.reset');
    Route::post('refresh-token', 'refreshToken')->name('token.refresh');
    Route::post('activate-account', 'activateAccount')->name('account.activate');
}); //END AUTHENTICATION
