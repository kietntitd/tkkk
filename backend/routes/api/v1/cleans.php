<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Clean\AuthController;
use App\Http\Controllers\Api\Clean\UserCustomerController;

/*
* API with Auth
*/
Route::group(['middleware' => ['auth:api_user']], function () {
    // Update must be use Post method because maybe upload file
     Route::apiResource('/users', UserCustomerController::class)->only(['index', 'store', 'show', 'update']);
});

/*
* API No Auth
*/

/**************************
 ***** AUTHENTICATION *****
 **************************/

 Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::middleware(['throttle:login'])->group(function () {
        Route::post('login', 'login');
    });
    Route::post('request-password', 'requestPassword')->name('password.request');
    Route::post('reset-password', 'resetPassword')->name('password.reset');
}); //END AUTHENTICATION