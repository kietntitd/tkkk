<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_stores', function (Blueprint $table) {
            $table->bigInteger('business_partner_id')->nullable()->after('pickup_circle');
            $table->bigInteger('area_id')->nullable()->after('pickup_circle');
            $table->bigInteger('job_id')->nullable()->after('pickup_circle');
            $table->bigInteger('job_type_id')->nullable()->after('pickup_circle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_stores', function (Blueprint $table) {
            $table->dropColumn('business_partner_id');
            $table->dropColumn('area_id');
            $table->dropColumn('job_id');
            $table->dropColumn('job_type_id');
        });
    }
};
