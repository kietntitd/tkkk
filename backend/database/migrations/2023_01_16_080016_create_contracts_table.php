<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('basic_contract_id')->nullable();
            $table->bigInteger('store_id')->nullable();
            $table->enum('status', ['new', 'using', 'ended', 'cancel'])->nullable();
            $table->enum('contract_type', ['sell', 'laundry', 'rent', 'rent_laundry', 'rent_pc']);
            $table->enum('manage_type', ['personal', 'size'])->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('clothes_change')->nullable();
            $table->string('pick_up_period')->nullable();
            $table->bigInteger('business_partner_id')->nullable();
            $table->integer('tax')->nullable();
            $table->decimal('wash_price', 10, 2)->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
};
