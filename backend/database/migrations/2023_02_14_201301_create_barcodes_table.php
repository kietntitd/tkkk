<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barcodes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('contract_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('staff_id')->nullable();
            $table->bigInteger('item_id')->nullable();
            $table->bigInteger('set_id')->nullable();
            $table->string('barcode')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('size')->nullable();
            $table->text('label')->nullable();
            $table->bigInteger('customer_department_id')->nullable();
            $table->enum('product_status', ['renting', 'unusing', 'washing'])->nullable();
            $table->enum('return_status', ['renting', 'unusing', 'washing'])->nullable();
            $table->text('change_reason')->nullable();
            $table->date('change_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barcodes');
    }
};
