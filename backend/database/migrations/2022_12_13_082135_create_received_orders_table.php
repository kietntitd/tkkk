<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('received_orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->nullable();
            $table->string('name')->nullable();
            $table->string('name_kana')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->string('phone',64)->nullable();
            $table->string('fax',64)->nullable();
            $table->string('tel',64)->nullable();
            $table->string('address')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('received_orders');
    }
};
