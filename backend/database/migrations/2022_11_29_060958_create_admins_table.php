<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('name_kana')->nullable();
            $table->tinyInteger('role')->comment('1: admin, 2: warehouse');
            $table->tinyInteger('is_active');
            $table->tinyInteger('is_system')->default(0)->comment('0: admin|superAdmin, 1: system (can create superAdmin)');
            $table->tinyInteger('is_super')->default(0)->comment('0: admin, 1: superAdmin (can create admin)');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
};
