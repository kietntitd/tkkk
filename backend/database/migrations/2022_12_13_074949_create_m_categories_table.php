<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('note')->nullable();
            $table->text('illustration_path')->nullable()->comment('Ảnh minh họa vị trí gắn label sản phẩm');
            $table->text('illustration_des')->nullable()->comment('Ghi chú của hình minh họa');
            $table->tinyInteger('is_active')->default(0)->comment('0: inactive - không hiển thị ở các màn list');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_categories');
    }
};
