<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs', function (Blueprint $table) {
            $table->dropColumn('department');
            $table->bigInteger('department_id')->nullable()->after('id');
            $table->bigInteger('contract_id')->nullable()->after('department_id');
            $table->date('dob')->nullable()->after('contract_id');
            $table->enum('status', ['active', 'inactive'])
            ->comment('Trạng thái nhân viên: đang sử dụng, nghỉ việc')
            ->after('dob');
            $table->text('remarks')->nullable()->after('status');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs', function (Blueprint $table) {
            $table->dropColumn('department_id');
            $table->dropColumn('contract_id');
            $table->dropColumn('dob');
            $table->dropColumn('status');
            $table->dropColumn('remarks');
            $table->string('department');
            $table->dropColumn('deleted_at');
        });
    }
};