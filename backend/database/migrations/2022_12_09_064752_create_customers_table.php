<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('email')->nullable();
            $table->string('code')->comment('Code định danh của customer');
            $table->string('ctplan_code', 64)->nullable()->comment('customer code ở hệ thống cũ CTPlaning');
            $table->string('name')->nullable();
            $table->string('name_kana')->nullable();
            $table->string('postal_code', 20)->nullable();
            $table->string('address1')->comment('tỉnh thành, quận huyện, số nhà');
            $table->string('address2')->nullable()->comment('tên tòa nhà, số phòng ');
            $table->string('fax', 20)->nullable();
            $table->string('tel', 20);
            $table->dateTime('invoice_date')->nullable()->comment('Ngày gửi hóa đơn thanh toán');
            $table->integer('pickup_circle')->nullable()->comment('số lần đến lấy ĐP/tuần');
            $table->string('label_type')->nullable();
            $table->text('note')->nullable();
            $table->string('cleaning_factory_code')->nullable()->comment('code công ty giặt là khi sử dụng dịch vụ');
            $table->tinyInteger('is_active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
