<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_products', function (Blueprint $table) {
            $table->id();
            $table->string('code', 64)->nullable()->comment('Mã SP');
            $table->string('name')->nullable();
            $table->string('name_kana')->nullable();
            $table->decimal('price', 10, 2)->comment('Giá gốc sản phẩm');
            $table->bigInteger('business_partner_id')->comment('id nhà máy sx');
            $table->string('factory_product_code', 64)->nullable()->comment('Mã sản phẩm nhà sản xuất');
            $table->string('factory_product_name')->nullable()->comment('Tên sản phẩm nhà sản xuất');
            $table->decimal('factory_product_price', 10, 2)->nullable()->comment('Đơn giá cung cấp từ nhà sản xuất');
            $table->string('customer_code', 64)->nullable()->comment('Mã khách hàng');
            $table->string('agency_code', 64)->nullable()->comment('Mã xác thực đại lý');
            $table->string('factory_code', 64)->nullable()->comment('Mã xác thực nhà máy');
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_products');
    }
};
