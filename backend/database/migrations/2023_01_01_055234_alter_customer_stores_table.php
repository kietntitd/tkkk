<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_stores', function (Blueprint $table) {
            $table->string('pickup_circle')->nullable()->after('address_2');
            $table->renameColumn('address_1', 'address1');
            $table->renameColumn('address_2', 'address2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_stores', function (Blueprint $table) {
            // $table->dropColumn('pickup_circle');
            // $table->dropColumn('address_1');
            // $table->dropColumn('address_2');
        });
    }
};
