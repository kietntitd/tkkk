<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_purchase_prices', function (Blueprint $table) {
            $table->bigInteger('parent_id')->nullable()->after('product_id');
            $table->date('start_date')->nullable()->after('wash_price');
            $table->date('end_date')->nullable()->after('start_date');
            $table->decimal('wash_price', 10, 2)->nullable()->change();
            $table->dropColumn('rent_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_purchase_prices', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->decimal('rent_price', 10, 2)->comment('Giá thuê sản phẩm theo nhà máy giặt là or kho hàng');
            $table->decimal('wash_price', 10, 2)->comment('Giá giặt sản phẩm theo nhà máy giặt là or kho hàng');
        });
    }
};
