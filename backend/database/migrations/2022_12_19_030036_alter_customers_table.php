<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('pickup_circle')->nullable()->comment('số lần đến lấy ĐP/tuần')->change();
            $table->string('invoice_date', 10)->nullable()->comment('Ngày gửi hóa đơn thanh toán')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('pickup_circle')->nullable()->comment('số lần đến lấy ĐP/tuần')->change();
            $table->dateTime('invoice_date')->nullable()->comment('Ngày gửi hóa đơn thanh toán')->change();
        });
    }
};
