<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_sale_prices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id');
            $table->bigInteger('product_id');
            $table->decimal('rent_price', 10, 2)->comment('Giá thuê sản phẩm theo từng khách hàng customer');
            $table->decimal('sell_price', 10, 2)->comment('Giá bán sản phẩm theo từng khách hàng customer');
            $table->decimal('wash_price', 10, 2)->comment('Giá giặt sản phẩm theo từng khách hàng customer');
            $table->bigInteger('parent_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_sale_prices');
    }
};
