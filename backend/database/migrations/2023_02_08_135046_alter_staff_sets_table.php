<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_sets', function (Blueprint $table) {
            $table->bigInteger('contract_id')->nullable();
            $table->bigInteger('quantity')->nullable();
            $table->bigInteger('department_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_sets', function (Blueprint $table) {
            $table->dropColumn('contract_id');
            $table->dropColumn('quantity');
            $table->dropColumn('department_id');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });
    }
};
