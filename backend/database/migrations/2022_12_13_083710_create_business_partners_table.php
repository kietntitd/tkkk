<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_partners', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('name_kana')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->string('tel', 20)->nullable();
            $table->string('fax', 20)->nullable();
            $table->string('postal_code', 20)->nullable();
            $table->string('ctplan_code', 64)->nullable();
            $table->string('code', 64)->nullable();
            $table->string('invoice_date', 10)->nullable();
            $table->string('billing_code', 64)->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_partners');
    }
};
