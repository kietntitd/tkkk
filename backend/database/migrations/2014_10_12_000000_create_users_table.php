<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('name');
            $table->string('name_kana')->nullable();
            $table->tinyInteger('is_active')->comment('Chưa active không thể login');
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('business_partner_id')->nullable()->comment('Thuộc công ty cho thuê đồng phục');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
