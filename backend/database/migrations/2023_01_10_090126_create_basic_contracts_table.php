<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_contracts', function (Blueprint $table) {
            $table->id();
            $table->string('code', 64)->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->enum('invoice_date', ['5', '10', '15', '20', '25', '月末']);
            $table->integer('payment_date')->nullable();
            $table->boolean('is_active')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_contracts');
    }
};
