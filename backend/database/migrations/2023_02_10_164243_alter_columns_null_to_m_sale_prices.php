<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_sale_prices', function (Blueprint $table) {
            $table->decimal('sell_price', 10, 2)->nullable()->change();
            $table->decimal('rent_price', 10, 2)->nullable()->change();
            $table->decimal('wash_price', 10, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_sale_prices', function (Blueprint $table) {
            $table->decimal('rent_price', 10, 2)->comment('Giá thuê sản phẩm theo từng khách hàng customer');
            $table->decimal('sell_price', 10, 2)->comment('Giá bán sản phẩm theo từng khách hàng customer');
            $table->decimal('wash_price', 10, 2)->comment('Giá giặt sản phẩm theo từng khách hàng customer');
        });
    }
};
