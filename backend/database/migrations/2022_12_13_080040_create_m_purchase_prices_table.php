<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_purchase_prices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('business_partner_id')->comment('nhà máy giặt là or kho hàng');
            $table->bigInteger('product_id');
            $table->decimal('rent_price', 10, 2)->comment('Giá thuê sản phẩm theo nhà máy giặt là or kho hàng');
            $table->decimal('wash_price', 10, 2)->comment('Giá giặt sản phẩm theo nhà máy giặt là or kho hàng');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_purchase_prices');
    }
};
