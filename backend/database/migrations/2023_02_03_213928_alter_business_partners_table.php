<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_partners', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('invoice_date');
            $table->dropColumn('billing_code');
            $table->dropColumn('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bussines_partners', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('invoice_date', 10)->nullable();
            $table->string('billing_code')->nullable();
            $table->text('note')->nullable();
        });
    }
};
