<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('pickup_circle');
            $table->dropColumn('cleaning_factory_code');
            $table->dropColumn('label_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('pickup_circle')->nullable();
            $table->string('cleaning_factory_code')->nullable();
            $table->string('label_type')->nullable();
        });
    }
};
