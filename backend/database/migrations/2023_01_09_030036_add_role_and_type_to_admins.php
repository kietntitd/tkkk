<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->tinyInteger('is_master')->default(0)->after('is_active');
            $table->tinyInteger('type')->default(1)->after('is_master');
            $table->dropColumn('role');
            $table->dropColumn('is_system');
            $table->dropColumn('is_super');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('is_master');
            $table->dropColumn('type');
        });
    }
};
