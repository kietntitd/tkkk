<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->string('label_name', 255)->nullable()->after('name_kana');
            $table->string('factory_name', 255)->nullable()->after('agency_code');
            $table->string('cleaning_factory_code', 64)->nullable()->after('factory_code');
            $table->bigInteger('category_id')->after('price');
            $table->tinyInteger('is_active')->default(0)->after('remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->dropColumn('label_name');
            $table->dropColumn('factory_name');
            $table->dropColumn('category_id');
            $table->dropColumn('is_active');
            $table->dropColumn('cleaning_factory_code');
        });
    }
};
