<?php

namespace Database\Seeders;

use Doctrine\DBAL\Schema\Schema;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalePriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sale_prices')->truncate();

        $sale_prices = [
            ['1', '1', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['2', '2', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['3', '3', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['4', '4', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['5', '5', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['6', '6', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['7', '7', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['8', '8', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['9', '9', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['10', '10', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['11', '11', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['12', '12', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['13', '13', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
            ['14', '14', '12.02','14.03', '15.04', '2022/12/12','2022/12/13', null],
        ];
        foreach ($sale_prices as $sale_price) {
            DB::table('m_sale_prices')->insert([
                'customer_id' => $sale_price[0],
                'product_id' => $sale_price[1],
                'rent_price' => $sale_price[2],
                'sell_price' => $sale_price[3],
                'wash_price' => $sale_price[4],
                'created_at' => $sale_price[5],
                'updated_at' => $sale_price[6],
                'deleted_at' => $sale_price[7],
            ]);
        }
    }
}
