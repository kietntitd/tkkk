<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\CustomerStore;

class CustomerStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerStore::factory()
            ->count(50)
            ->create();
    }
}
