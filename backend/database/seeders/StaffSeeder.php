<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Staff;
use App\Models\SetProduct;

class StaffSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::factory()
            ->count(20)
            ->create();

    }
}
