<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\StoreAttribute;

class StoreAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StoreAttribute::factory()
            ->count(40)
            ->create();
    }
}
