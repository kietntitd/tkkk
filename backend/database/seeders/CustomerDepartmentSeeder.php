<?php

namespace Database\Seeders;

use App\Models\CustomerDepartment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomerDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerDepartment::factory()
            ->count(50)
            ->create();
    }
}
