<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\BusinessPartner;

class BusinessPartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessPartner::factory()
            ->count(50)
            ->create();
    }
}
