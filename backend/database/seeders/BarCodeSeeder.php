<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\BarCode;

class BarCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BarCode::factory()
            ->count(50)
            ->create();
    }
}
