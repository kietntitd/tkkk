<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Set;
use App\Models\SetProduct;

class SetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Set::factory()
            ->count(20)
            ->create();
        SetProduct::factory()
            ->count(20)
            ->create();
    }
}
