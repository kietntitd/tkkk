<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\StaffSet;

class StaffSetSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StaffSet::factory()
            ->count(20)
            ->create();

    }
}
