<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'admin',
            'name_kana' => 'admin',
            'email' => 'admin@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 1,
            'is_master' => 1,
            'is_active' => 1,
        ]);
        Admin::create([
            'name' => 'admin1',
            'name_kana' => 'admin1',
            'email' => 'admin1@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 1,
            'is_master' => 1,
            'is_active' => 1,
        ]);
        Admin::create([
            'name' => 'admin2',
            'name_kana' => 'admin2',
            'email' => 'admin2@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 1,
            'is_master' => 1,
            'is_active' => 1,
        ]);
    }
}
