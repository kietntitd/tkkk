<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\BusinessPartner;
use App\Models\Customer;
use App\Models\CustomerStore;
use App\Services\BusinessPartnerService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            AdminSeeder::class,
            CategorySeeder::class,
            ProductSeeder::class,
            SetSeeder::class,
            CustomerSeeder::class,
            BusinessPartnerSeeder::class,
            SalePriceSeeder::class,
            ItemSeeder::class,
            ContractSeeder::class,
            CustomerStoreSeeder::class,
			//ItemSeeder::class,
            //BarCodeSeeder::class,
            //ContractSeeder::class,
            //CustomerDepartmentSeeder::class,
            //CustomerStoreSeeder::class,
            //StaffSetSeeder::class,
            //StaffSeeder::class,        
        ]);
    }
}
