<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::factory()
        //     ->count(50)
        //     ->create();
        User::create([
            'name' => 'admin',
            'name_kana' => 'admin',
            'email' => 'admin@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 3,
            'is_master' => 1,
            'is_active' => 1,
        ]);
        User::create([
            'name' => 'admin1',
            'name_kana' => 'admin1',
            'email' => 'admin1@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 1,
            'is_master' => 2,
            'is_active' => 1,
        ]);
        User::create([
            'name' => 'admin2',
            'name_kana' => 'admin2',
            'email' => 'admin2@thefirstone.jp',
            'password' => 'Password@1234',
            'type' => 1,
            'is_master' => 0,
            'is_active' => 1,
        ]);
    }
}
