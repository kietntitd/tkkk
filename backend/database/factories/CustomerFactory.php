<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'email' => fake()->email(),
            'code' => fake()->numberBetween(10000000000, 99999999999),
            'ctplan_code' => null,
            'name' =>  "Customer" . fake()->numberBetween(1, 100),
            'name_kana' => 'ｻﾝﾌﾟﾙ',
            'postal_code' => '123-4567',
            'address1' => fake()->address(),
            'address2' => fake()->address(),
            'fax' => '123-4567-12345',
            'tel' => '123-4567-12345',
            'invoice_date' => fake()->randomElement(['5', '10', '15', '20', '25', '末尾']),
            'note' => fake()->text(),
            'is_active' => fake()->numberBetween(0, 1),
        ];
    }
}
