<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class CustomerStoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => "Store" . fake()->numberBetween(1, 100),
            'code' => fake()->text(10),
            'customer_id' => fake()->numberBetween(1, 20),
            'business_partner_id' => fake()->numberBetween(1, 20),
            'area_id' => fake()->numberBetween(1, 3),
            'job_id' => fake()->numberBetween(1, 3),
            'job_type_id' => fake()->numberBetween(1, 3),
            'fax' => '123-4567-12345',
            'tel' => '123-4567-12345',
            'address1' => fake()->address(),
            'address2' => fake()->address(),
            'pickup_circle' => '{"monday":true,"tuesday":false,"wednesday":false,"thursday":false,"friday":false,"saturday":false,"sunday":true}'
        ];
    }
}
