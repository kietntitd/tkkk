<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BusinessPartner>
 */
class BusinessPartnerFactory extends Factory
{
    // name 
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'business_partner_' . random_int(1, 100),
            'name_kana' => 'ｻﾝﾌﾟﾙ',
            'email' => fake()->email(),
            'is_active' => '1',
            'type' => random_int(1, 3),
            'address1' => 'address1',
            'address2' => 'address2',
            'tel' => '123-4567-12345',
            'fax' => '123-4567-12345',
            'postal_code' => '123-4567',
            'ctplan_code' => '1234567890',
            'code' => 'code' . random_int(1, 100),
        ];
    }
}
