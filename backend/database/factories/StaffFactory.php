<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Staff>
 */
class StaffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        $dob = $this->faker->dateTimeBetween('-5 years', 'now');
        $start_date = $this->faker->dateTimeBetween('-5 years', 'now');
        $end_date = $this->faker->optional()->dateTimeBetween($start_date, 'now');

        return [
            'department_id' => $this->faker->numberBetween(1, 20),
            'contract_id' => $this->faker->numberBetween(1, 20),
            'dob' => $dob,
            'status' => $this->faker->randomElement(['active', 'inactive']),
            'remarks' => $this->faker->text(),
            'staff_code' => $this->faker->unique()->regexify('[A-Za-z0-9]{10}'),
            'name' => $this->faker->name(),
            'gender' => $this->faker->numberBetween(0, 1),
            'customer_id' => $this->faker->numberBetween(1, 20),
        ];
    }
}
