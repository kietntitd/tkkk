<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Set>
 */
class StaffSetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        $start_date = fake()->dateTimeBetween('-10 years', 'now');
        $end_date = fake()->dateTimeBetween($start_date, '+10 days');
        
        return [
            'set_id' => fake()->numberBetween(1, 20),
            'staff_id' => fake()->numberBetween(1, 20),
            'size' => fake()->numberBetween(1, 10),
            'created_at' => fake()->dateTime(),
            'updated_at' => fake()->dateTime(),
            'contract_id' => fake()->numberBetween(1, 20),
            'quantity' => fake()->numberBetween(5, 200),
            'department_id' => fake()->numberBetween(1, 20),
            'start_date' => $start_date->format('Y-m-d'),
            'end_date' => $end_date->format('Y-m-d'),
        ];
    }
}
