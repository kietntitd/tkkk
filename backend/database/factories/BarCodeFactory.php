<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class BarCodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $size = ['S', 'M', 'L', 'XL', 'XXL'];
        $start_date = fake()->dateTimeBetween('-10 years', 'now');
        $change_date = fake()->dateTimeBetween($start_date, 'now');
        $end_date = fake()->dateTimeBetween($change_date, '+10 days');

        return [
            'contract_id' => fake()->numberBetween(1, 20),
            'customer_id' => fake()->numberBetween(1, 20),
            'staff_id' => fake()->numberBetween(1, 20),
            'item_id' => fake()->numberBetween(1, 20),
            'customer_department_id' => fake()->numberBetween(1, 20),
            'set_id' => fake()->numberBetween(1, 20),
            'product_id' => fake()->numberBetween(1, 20),
            'barcode' => fake()->regexify('[0-9]{8}'),
            'size' => $this->faker->randomElement($size),
            'label' => fake()->text(10),
            'product_status' => random_int(1, 3),
            'return_status' => random_int(1, 3),
            'change_reason' => fake()->text(10),
            'change_date' => $change_date->format('Y-m-d'),
            'start_date' => $start_date->format('Y-m-d'),
            'end_date' => $end_date->format('Y-m-d'),
        ];
    }
}
