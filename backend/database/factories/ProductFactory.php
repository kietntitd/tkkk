<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'code' => 'P' . fake()->randomNumber(5),
            'name' => fake()->name(),
            'name_kana' => fake()->name(),
            'display_label' => fake()->name(),
            'price' => fake()->randomNumber(5),
            'category_id' => fake()->numberBetween(1, 20),
            'factory_product_code' => fake()->uuid(),
            'factory_product_name' => fake()->name(),
            'factory_product_price' => fake()->randomNumber(5),
            // 'factory_name' => fake()->name(),
            // 'factory_code' => fake()->uuid(),
            'is_active' => fake()->numberBetween(0, 1),
            'business_partner_id' => fake()->numberBetween(1, 20),
        ];
    }
}
