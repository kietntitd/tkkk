<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $productIds = DB::table('m_products')->pluck('id')->toArray();
        $size = ['S', 'M', 'L', 'XL', 'XXL'];
        return [
            'product_id' => $this->faker->randomElement($productIds),
            'size' => $this->faker->randomElement($size),
            'warning_quantity' => $this->faker->numberBetween(1, 100),
        ];
    }
}
