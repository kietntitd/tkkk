<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ContractFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $start_date = fake()->dateTimeBetween('-10 years', 'now');
        $end_date = fake()->dateTimeBetween($start_date, '+10 days');

        return [
			//'contract_type'=> fake()->numberBetween(1, 4),
            //'manage_type'=> fake()->numberBetween(1, 2),
            'status'=> fake()->numberBetween(1, 3),
            'basic_contract_id' => fake()->numberBetween(1, 50),
            'customer_id' => fake()->numberBetween(1, 50),
            'store_id' => fake()->numberBetween(1, 50),
            //'status' => fake()->randomElement(['new', 'using', 'ended', 'cancel']),
            'contract_type' => fake()->randomElement(['sell', 'laundry', 'rent', 'rentLaundry']),
            'manage_type' => fake()->randomElement(['personal', 'size']),
            'start_date' => $start_date->format('Y-m-d'),
            'end_date' => $end_date->format('Y-m-d'),
            'clothes_change' => fake()->numberBetween(1, 10),
            'pick_up_period' => fake()->text,
            'business_partner_id' => fake()->numberBetween(1, 100),
            'tax' => fake()->numberBetween(1, 10),
            'wash_price' => fake()->randomFloat(2, 1, 100),
            'remarks' => fake()->text,
        ];
    }
}
