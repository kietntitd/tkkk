<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CustomerDepartment>
 */
class CustomerDepartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $customerId = DB::table('customers')->pluck('id')->toArray();
        $storeId = DB::table('customer_stores')->pluck('id')->toArray();
        return [
            'name' => "Phòng " . fake()->numberBetween(1, 100),
            'code' => "D" . fake()->numberBetween(1, 100),
            'customer_id' => $this->faker->randomElement($customerId),
            'store_id' => $this->faker->randomElement($storeId),
        ];
    }
}
