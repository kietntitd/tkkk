<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as BaseResetPassword;

class NewPasswordNotification extends BaseResetPassword
{

    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $user)
    {
        parent::__construct($token);
        $this->user = $user;
    }

    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->markdown('mail.newPassword', [
                'user' => $this->user,
                'newPasswordUrl' => $url,
            ]);
    }
}
