<?php

namespace App\Repositories;

/**
 * Interface ContractSetRepositoryInterface
 *
 * @package App\Repositories
 */
interface ContractSetRepositoryInterface extends BaseRepositoryInterface
{
}
