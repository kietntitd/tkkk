<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories
 */
interface UserCustomerRepositoryInterface extends BaseRepositoryInterface
{
}
