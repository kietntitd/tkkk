<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\SalePrice;
use App\Repositories\SalePriceRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class SalePriceRepository
 *
 * @package App\Repositories\Impl
 *
 * @property SalePrice $model
 */
class SalePriceRepository extends BaseRepository implements SalePriceRepositoryInterface
{

    /**
     * SalePriceRepository constructor.
     * @param SalePrice $model
     */
    public function __construct(SalePrice $model)
    {
        parent::__construct($model);
    }

    /**
     * Check Sale Price isExits Product And Customer
     *
     * @param $productId
     * @param $customerId
     * @return mixed
     */
    public function isExitsProductAndCustomer($productId, $customerId)
    {
        return $this->model
            ->where('product_id', $productId)
            ->where('customer_id', $customerId)
            ->whereNull('parent_id')
            ->exists();
    }

    /**
     * Get list model items with pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $currentDate = Carbon::now()->format(DATE_FORMAT);
        $subQuery = $this->newQuery()->withoutGlobalScope('defaultOrder');
        $subQuery->selectRaw("max(id) as id");
        $subQuery->whereNotNull('parent_id')
            ->where('start_date', '<=', $currentDate)
            ->where(function ($nestedQuery) use ($currentDate) {
                $nestedQuery->where('end_date', '>=', $currentDate)
                    ->whereNull('end_date', 'or');
            })->orWhere('start_date', '<=', $currentDate)
            ->groupBy(['customer_id', 'product_id']);

        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFilters()));
        $query->select(['m_sale_prices.*'])->from('m_sale_prices', 'm_sale_prices')
            ->joinSub($subQuery, 'msp2', 'm_sale_prices.id', '=', 'msp2.id');
        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->withCount($this->getCountRelations($criteria))
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }

    /**
     * Find model with key business
     *
     * @param $relations
     * @param $keys
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Support\Collection
     */
    public function findWithKeyBusiness($relations, $keys)
    {
        return $this->model->with($relations)
            ->where('customer_id', $keys['customer_id'])
            ->where('product_id', $keys['product_id'])
            ->orderBy('id')
            ->get();
    }

    /**
     * Get Price By Date And Product
     *
     * @param $productId
     * @param $date
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]|mixed
     */
    public function getPriceByDateAndProduct($productId, $date = null)
    {
        if (!$date) {
            $date = Carbon::now()->format(DATE_FORMAT);
        }
        $subQuery = $this->newQuery()->withoutGlobalScope('defaultOrder');
        $subQuery->selectRaw("max(id) as id");
        $subQuery->whereNotNull('parent_id')
            ->where('start_date', '<=', $date)
            ->where(function ($nestedQuery) use ($date) {
                $nestedQuery->where('end_date', '>=', $date)
                    ->whereNull('end_date', 'or');
            })->orWhere('start_date', '<=', $date)
            ->groupBy(['customer_id', 'product_id']);

        $query = $this->newQuery();
        $query->select(['m_sale_prices.*'])->from('m_sale_prices', 'm_sale_prices')
            ->joinSub($subQuery, 'msp2', 'm_sale_prices.id', '=', 'msp2.id')
            ->where('m_sale_prices.product_id', '=', $productId);
        return $query->get();
    }

    /**
     * Check exist product in table m_sales_prices
     *
     * @param $productId
     * @return mixed
     */
    public function isExistProduct($productId)
    {
        return $this->model
            ->where('product_id', $productId)
            ->whereNull('deleted_at')
            ->exists();
    }
}
