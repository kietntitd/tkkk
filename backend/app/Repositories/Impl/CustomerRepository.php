<?php

namespace App\Repositories\Impl;

use App\Models\Customer;
use App\Repositories\CustomerRepositoryInterface;

/**
 * Class CustomerRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Customer $model
 */
class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    /**
     * CustomerRepository constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     */
    public function isExistsCustomer($customerId)
    {
        return $this->model
            ->where('id', $customerId)
            ->where('is_active', '=', 1)
            ->exists();
    }
    /**
     * Join table basic_contracts check customer exists contracts active
     */
    public function isExistsCustomerContract($customerId)
    {
        return $this->model
            ->join('basic_contracts', 'basic_contracts.customer_id', '=', 'customers.id')
            ->where('customers.id', $customerId)
            ->where('basic_contracts.is_active', 1)
            ->whereNull('customers.deleted_at')
            ->exists();
    }
}
