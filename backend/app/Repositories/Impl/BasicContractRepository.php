<?php

namespace App\Repositories\Impl;

use App\Models\BasicContract;
use App\Repositories\BasicContractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class BasicContractRepository
 *
 * @package App\Repositories\Impl
 *
 * @property BasicContract $model
 */
class BasicContractRepository extends BaseRepository implements BasicContractRepositoryInterface
{
    /**
     * CustomerRepository constructor.
     *
     * @param BasicContract $model
     */
    public function __construct(BasicContract $model)
    {
        parent::__construct($model);
    }

    /**
     * @Override
     */
    public function create(array $data): Model
    {
        $lastModel = DB::table('basic_contracts')->find(DB::table('basic_contracts')->max('id'));
        if ($lastModel) {
            $data['id'] = $lastModel->id;
        } else {
            $data['id'] = 1;
        }
        return parent::create($data);
    }
}
