<?php

namespace App\Repositories\Impl;

use App\Models\Set;
use App\Repositories\SetRepositoryInterface;

use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * Class SetRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Set $model
 */
class SetRepository extends BaseRepository implements SetRepositoryInterface
{
    /**
     * SetRepository constructor.
     * 
     * @param Set $model
     * 
     */
    public function __construct(Set $model)
    {
        parent::__construct($model);
    }
}
