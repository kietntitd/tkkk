<?php

namespace App\Repositories\Impl;

use App\Models\StoreAttribute;
use App\Repositories\StoreAttributeRepositoryInterface;

class StoreAttributeRepository extends BaseRepository implements StoreAttributeRepositoryInterface
{
    /**
     * StoreAttributeRepository constructor.
     *
     * @param StoreAttribute $model
     *
     */
    public function __construct(StoreAttribute $model)
    {
        parent::__construct($model);
    }
}
