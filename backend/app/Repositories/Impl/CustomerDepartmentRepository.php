<?php

namespace App\Repositories\Impl;

use App\Models\CustomerDepartment;
use App\Repositories\CustomerDepartmentRepositoryInterface;

/**
 * Class CustomerDepartmentRepository
 *
 * @package App\Repositories\Impl
 *
 * @property CustomerDepartment $model
 */
class CustomerDepartmentRepository extends BaseRepository implements CustomerDepartmentRepositoryInterface
{
    /**
     * CustomerDepartmentRepository constructor.
     *
     * @param CustomerDepartment $model
     */
    public function __construct(CustomerDepartment $model)
    {
        parent::__construct($model);
    }

    /**
     * Join table staffs and customer_departments to validate exits staff in department
     */
    public function isExistsStaffInDepartment($departmentId)
    {
        return $this->model
            ->join('staffs', 'staffs.department_id', '=', 'customer_departments.id')
            ->where('customer_departments.id', $departmentId)
            ->whereNull('customer_departments.deleted_at')
            ->exists();
    }


}
