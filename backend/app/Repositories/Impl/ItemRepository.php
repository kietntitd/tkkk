<?php

namespace App\Repositories\Impl;

use App\Models\Item;
use App\Repositories\ItemRepositoryInterface;

/**
 * Class ItemRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Item $model
 */
class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{

    /**
     * ItemRepository constructor.
     * @param Item $model
     */
    public function __construct(Item $model)
    {
        parent::__construct($model);
    }

    /**
     * Check isExist by product_id and size
     */
    public function isExistProductAndSize($productId, $size)
    {
        return $this->model->where('product_id', $productId)->where('size', $size)->exists();
    }

    /**
     * Check isExist by product_id
     */
    public function isExistProduct($productId)
    {
        return $this->model->where('product_id', $productId)->exists();
    }

    /**
     * Find data id init with relation product
     */
    public function findDataIdInit($id) :object
    {
        return $this->model->with('product')->find($id);
    }

}
