<?php

namespace App\Repositories\Impl;

use App\Models\BusinessPartnersRel;
use App\Repositories\BusinessPartnerRelRepositoryInterface;

/**
 * Class BusinessPartnerRelRepository
 *
 * @package App\Repositories\Impl
 *
 * @property BusinessPartnersRel $model
 */
class BusinessPartnerRelRepository extends BaseRepository implements BusinessPartnerRelRepositoryInterface
{
    /**
     * BusinessPartnerRelRepository constructor.
     *
     * @param BusinessPartnersRel $model
     */
    public function __construct(BusinessPartnersRel $model)
    {
        parent::__construct($model);
    }

    public function saveMany($businessPartnerId, $businessPartnersRel)
    {
        $currentTime = new \DateTime();
        $this->model->insert(array_map(function ($businessPartner) use ($businessPartnerId, $currentTime) {
            return array(
                'business_partner_id' => $businessPartnerId,
                'children_id' => $businessPartner['children_id'],
                'created_at' => $currentTime,
                'updated_at' => $currentTime,
            );
        }, $businessPartnersRel));
    }

   /**
    * Update many BusinessPartnerRel
    */
   public function updateMany($businessPartner, $updateBusinessPartnersRel)
   {
       $currentBusinessPartnersRel = $businessPartner->businessPartnersRels;
       $iterator = max([count($currentBusinessPartnersRel), count($updateBusinessPartnersRel)]);
       for ($i = 0; $i < $iterator; $i++) {
           if (empty($currentBusinessPartnersRel[$i])) {
               $this->model->create([
                   'business_partner_id' => $businessPartner->id,
                   'children_id' => $updateBusinessPartnersRel[$i]['children_id'],
               ]);
               continue;
           }

           if (empty($updateBusinessPartnersRel[$i])) {
               $this->model->destroy($currentBusinessPartnersRel[$i]['id']);
               continue;
           }

           $this->model->where('id', $currentBusinessPartnersRel[$i]['id'])->update([
               'children_id' => $updateBusinessPartnersRel[$i]['children_id'],
           ]);
       }
   }

}
