<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\BaseModel;
use App\Models\ContractSet;
use App\Repositories\ContractSetRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class ContractSetRepository extends BaseRepository implements ContractSetRepositoryInterface
{
    /**
     * ContractSetRepository constructor.
     *
     * @param ContractSet $model
     *
     */
    public function __construct(ContractSet $model)
    {
        parent::__construct($model);
    }

    /**
     * @Override
     */
    public function deleteByContractId($contractId): void
    {
        $collection = ContractSet::where('contract_id', $contractId)->get(['id']);
        ContractSet::destroy($collection->toArray());
    }

    public function findByContractId($contractId)
    {
        return ContractSet::where('contract_id', $contractId)->get(['id']);
    }

    /**
     * Check using set in table contract_sets
     *
     * @param $setId
     * @return mixed
     */
    public function isUsingSet($setId)
    {
        return $this->model
            ->where('set_id', $setId)
            ->whereNull('deleted_at')
            ->exists();
    }
}
