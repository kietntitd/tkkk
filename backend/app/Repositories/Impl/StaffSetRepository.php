<?php

namespace App\Repositories\Impl;

use App\Models\StaffSet;
use App\Repositories\StaffSetRepositoryInterface;

use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * Class SetRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Set $model
 */
class StaffSetRepository extends BaseRepository implements StaffSetRepositoryInterface
{
    /**
     * SetRepository constructor.
     * 
     * @param Set $model
     * 
     */
    public function __construct(Set $model)
    {
        parent::__construct($model);
    }
}
