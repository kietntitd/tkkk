<?php

namespace App\Repositories\Impl;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;

/**
 * Class CategoryRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Category $model
 */
class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * CategoryRepository constructor.
     *
     * @param Category $model
     *
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * @param $categoryId
     * @return mixed
     */

    public function isExistsCategory($categoryId)
    {
        return $this->model
            ->where('id', $categoryId)
            ->where('is_active', '=', 1)
            ->exists();
    }
}
