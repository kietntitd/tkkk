<?php

namespace App\Repositories\Impl;

use App\Models\SetProduct;
use App\Repositories\SetProductRepositoryInterface;

/**
 * Class SetProductRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Product $model
 */
class SetProductRepository extends BaseRepository implements SetProductRepositoryInterface
{
    /**
     * SetProductRepository constructor.
     *
     * @param SetProduct $model
     *
     */
    public function __construct(SetProduct $model)
    {
        parent::__construct($model);
    }

    public function saveMany($setId, $products)
    {
        $currentTime = new \DateTime();
        $this->model->insert(array_map(function ($product) use ($setId, $currentTime) {
            return array(
                'set_id' => $setId,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity'],
                'created_at' => $currentTime,
                'updated_at' => $currentTime,
            );
        }, $products));
    }

    public function updateMany($set, $updateSetProducts)
    {
        $currentSetProducts = $set->setProduct;
        $iterator = max([count($currentSetProducts), count($updateSetProducts)]);
        for ($i = 0; $i < $iterator; $i++) {
            if (empty($currentSetProducts[$i])) {
                $this->model->create([
                    'set_id' => $set->id,
                    'product_id' => $updateSetProducts[$i]['product_id'],
                    'quantity' => $updateSetProducts[$i]['quantity'],
                ]);
                continue;
            }

            if (empty($updateSetProducts[$i])) {
                $this->model->destroy($currentSetProducts[$i]['id']);
                continue;
            }

            $this->update($currentSetProducts[$i]['id'], [
                'set_id' => $set->id,
                'product_id' => $updateSetProducts[$i]['product_id'],
                'quantity' => $updateSetProducts[$i]['quantity'],
            ]);
        }
    }

    public function isExistInSet($productId)
    {
        return $this->model
            ->where('product_id', $productId)
            ->whereNull('deleted_at')
            ->exists();
    }

    public function deleteBySetId($setId)
    {
        $collection = SetProduct::where('set_id', $setId)->get(['id']);
        SetProduct::destroy($collection->toArray());
    }
}
