<?php

namespace App\Repositories\Impl;

use App\Models\BusinessPartner;
use App\Repositories\BusinessPartnerRepositoryInterface;

/**
 * Class BusinessPartnerRepository
 *
 * @package App\Repositories\Impl
 *
 * @property BusinessPartner $model
 */
class BusinessPartnerRepository extends BaseRepository implements BusinessPartnerRepositoryInterface
{
    /**
     * BusinessPartnerRepository constructor.
     *
     * @param BusinessPartner $model
     */
    public function __construct(BusinessPartner $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     */
    public function isExistsBusinessPartner($businessPartnerId)
    {
        return $this->model
            ->where('id', $businessPartnerId)
            ->where('is_active', '=', 1)
            ->exists();
    }

    public function disableSelfRecord($businessPartnerId)
    {
        return $this->model
            ->whereNot('id', $businessPartnerId);
    }
}
