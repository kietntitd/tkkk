<?php

namespace App\Repositories\Impl;

use App\Models\BarCode;
use App\Repositories\BarCodeRepositoryInterface;

class BarCodeRepository extends BaseRepository implements BarCodeRepositoryInterface
{
    /**
     * BarCodeRepository constructor.
     *
     * @param BarCode $model
     */
    public function __construct(BarCode $model)
    {
        parent::__construct($model);
    }

    /**
     * Check using set in table barcodes
     *
     * @param $setId
     * @return mixed
     */
    public function isUsingSet($setId)
    {
        return $this->model
            ->where('set_id', $setId)
            ->whereNull('deleted_at')
            ->exists();
    }
}
