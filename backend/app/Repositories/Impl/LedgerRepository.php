<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\CustomerStore;
use App\Repositories\LedgerRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class CustomerStoreRepository
 *
 * @package App\Repositories\Impl
 *
 * @property CustomerStore $model
 */
class LedgerRepository extends BaseRepository implements LedgerRepositoryInterface
{
    /**
     * CustomerRepository constructor.
     *
     * @param CustomerStore $model
     */
    public function __construct(CustomerStore $model)
    {
        parent::__construct($model);
    }

    /**
     * @Override
     */
    public function getDataOptions(?Criteria $criteria = null): Collection
    {
        if (!$criteria) {
            $criteria = new Criteria();
        }
        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFiltersDropdown()));
        if (!empty($criteria->getSelect())) {
            $query->select($criteria->getSelect());
        }

        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->withCount($this->getCountRelations($criteria))
            ->get();
    }

    public function listStaff(Criteria $criteria): LengthAwarePaginator
    {

        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFilters()));

        if (!empty($criteria->getSelect())) {
            $query->select($criteria->getSelect());
            dd($query);
        }

        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->first()->customer->staff()
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }
}
