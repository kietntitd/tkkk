<?php

namespace App\Repositories\Impl;

use App\Models\Product;
use App\Repositories\ProductRepositoryInterface;

/**
 * Class ProductRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Product $model
 */
class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     *
     */
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     */
    public function isExistsProduct($productId)
    {
        return $this->model
            ->where('id', $productId)
            ->where('is_active', '=', 1)
            ->exists();
    }

}
