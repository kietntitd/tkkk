<?php

namespace App\Repositories\Impl;

use App\Models\Contract;
use App\Repositories\ContractRepositoryInterface;


class ContractRepository extends BaseRepository implements ContractRepositoryInterface
{
    /**
     * ContractRepository constructor.
     *
     * @param Contract $model
     *
     */
    public function __construct(Contract $model)
    {
        parent::__construct($model);
    }

    /**
     * @Override
     */
    public function deleteByBasicContractId($idBasicContract): void
    {
        $collection = Contract::where('basic_contract_id', $idBasicContract)->get(['id']);
        Contract::destroy($collection->toArray());
    }


    public function hasUsingContract($contractId): bool
    {
        return $this->model
            ->where('id', $contractId)
            ->where('status', 'using')->exists();
    }
}
