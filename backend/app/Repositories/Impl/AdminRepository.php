<?php

namespace App\Repositories\Impl;

use App\Models\Admin;
use App\Repositories\AdminRepositoryInterface;

/**
 * Class AdminRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Admin $model
 */
class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    /**
     * AdminRepository constructor.
     *
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        parent::__construct($model);
    }

    /**
     * Get model by email
     *
     * @param string $email
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * @inheritDoc
     */
    public function isUniqueEmail(array $data, string $mode): bool
    {
        if ($mode === 'edit') {
            return $this->model
                ->where('email', $data['email'])
                ->whereNull('deleted_at')
                ->whereNot('id', $data['id'])
                ->exists();
        }
        return $this->model
            ->where('email', $data['email'])
            ->whereNull('deleted_at')
            ->exists();
    }
}
