<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\UserCustomer;
use App\Repositories\UserCustomerRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class UserRepository
 *
 * @package App\Repositories\Impl
 *
 * @property UserCustomer $model
 */
class UserCustomerRepository extends BaseRepository implements UserCustomerRepositoryInterface
{
    /**
     * UserRepository constructor.
     * @param UserCustomer $model
     */
    public function __construct(UserCustomer $model)
    {
        parent::__construct($model);
    }

}
