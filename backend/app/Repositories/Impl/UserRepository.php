<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class UserRepository
 *
 * @package App\Repositories\Impl
 *
 * @property User $model
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * Get list model items with pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFilters()));
        if (!empty($criteria->getSelect())) {
            $query->select($criteria->getSelect());
        }
        if (in_array('customer', $this->getRelations($criteria))) {
            $query->select(['users.*', 'customers.name as company_name']);
            $query->leftJoin('customers', 'customers.id', '=', 'users.customer_id');
        } else {
            $query->select(['users.*', 'business_partners.name as company_name']);
            $query->leftJoin('business_partners', 'business_partners.id', '=', 'users.business_partner_id');
        }
        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->withCount($this->getCountRelations($criteria))
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }

    /**
     * @inheritDoc
     */
    public function isUniqueEmail(array $data, string $mode): bool
    {
        if ($mode === 'edit') {
            return $this->model
                ->where('email', $data['email'])
                ->whereNull('deleted_at')
                ->whereNot('id', $data['id'])
                ->exists();
        }
        return $this->model
            ->where('email', $data['email'])
            ->whereNull('deleted_at')
            ->exists();
    }

    /**
     * Get model by email
     *
     * @param string $email
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->get();
    }
}
