<?php

namespace App\Repositories\Impl;

use App\Models\WarehouseShelf;
use App\Repositories\WarehouseShelfRepositoryInterface;

use Illuminate\Support\Carbon;

/**
 * Class WhShelfRepository
 *
 * @package App\Repositories\Impl
 *
 * @property WarehouseShelf $model
 */
class WarehouseShelfRepository extends BaseRepository implements WarehouseShelfRepositoryInterface
{
    /**
     * WhAreaRepository constructor.
     * 
     * @param WarehouseShelf $model
     * 
     */
    public function __construct(WarehouseShelf $model)
    {
        parent::__construct($model);
    }
}
