<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\PurchasePrice;
use App\Repositories\PurchasePriceRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PurchasePriceRepository
 *
 * @package App\Repositories\Impl
 *
 * @property PurchasePrice $model
 */
class PurchasePriceRepository extends BaseRepository implements PurchasePriceRepositoryInterface
{
    /**
     * PurchasePriceRepository constructor.
     *
     * @param PurchasePrice $model
     *
     */
    public function __construct(PurchasePrice $model)
    {
        parent::__construct($model);
    }

    /**
     * Get list model items with pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $currentDate = Carbon::now()->format(DATE_FORMAT);
        $subQuery = $this->newQuery()->withoutGlobalScope('defaultOrder');
        $subQuery->selectRaw("max(id) as id");
        $subQuery->whereNotNull('parent_id')
            ->where('start_date', '<=', $currentDate)
            ->where(function ($nestedQuery) use ($currentDate) {
                $nestedQuery->where('end_date', '>=', $currentDate)
                    ->whereNull('end_date', 'or');
            })->orWhere('start_date', '<=', $currentDate)
            ->groupBy(['business_partner_id', 'product_id']);

        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFilters()));
        $query->select(['m_purchase_prices.*'])->from('m_purchase_prices', 'm_purchase_prices')
            ->joinSub($subQuery, 'mpp2', 'm_purchase_prices.id', '=', 'mpp2.id');
        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->withCount($this->getCountRelations($criteria))
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }

    /**
     * @inheritDoc
     */
    public function isExitsProductAndBusinessPartner($productId, $businessPartnerId)
    {
        return $this->model
            ->where('product_id', $productId)
            ->where('business_partner_id', $businessPartnerId)
            ->whereNull('parent_id')
            ->exists();
    }

    /**
     * @inheritDoc
     */
    public function findWithKeyBusiness($relations, $keys)
    {
        return $this->model->with($relations)
            ->where('business_partner_id', $keys['business_partner_id'])
            ->where('product_id', $keys['product_id'])
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Get Purchase Price By Date And Product
     *
     * @param $productId
     * @param $date
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]|mixed
     */
    public function getPriceByDateAndProduct($productId, $date = null)
    {
        if (!$date) {
            $date = Carbon::now()->format(DATE_FORMAT);
        }
        $subQuery = $this->newQuery()->withoutGlobalScope('defaultOrder');
        $subQuery->selectRaw("max(id) as id");
        $subQuery->whereNotNull('parent_id')
            ->where('start_date', '<=', $date)
            ->where(function ($nestedQuery) use ($date) {
                $nestedQuery->where('end_date', '>=', $date)
                    ->whereNull('end_date', 'or');
            })->orWhere('start_date', '<=', $date)
            ->groupBy(['business_partner_id', 'product_id']);

        $query = $this->newQuery();
        $query->select(['m_purchase_prices.*'])->from('m_purchase_prices', 'm_purchase_prices')
            ->joinSub($subQuery, 'mpp2', 'm_purchase_prices.id', '=', 'mpp2.id')
            ->where('m_purchase_prices.product_id', '=', $productId);
        return $query->get();
    }

    /**
     * Check exist product in table m_purchase_prices
     *
     * @param $productId
     * @return mixed
     */
    public function isExistProduct($productId)
    {
        return $this->model
            ->where('product_id', $productId)
            ->whereNull('deleted_at')
            ->exists();
    }
}
