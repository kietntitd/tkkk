<?php

namespace App\Repositories\Impl;

use App\Http\Parameters\Criteria;
use App\Models\CustomerStore;
use App\Repositories\CustomerStoreRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class CustomerStoreRepository
 *
 * @package App\Repositories\Impl
 *
 * @property CustomerStore $model
 */
class CustomerStoreRepository extends BaseRepository implements CustomerStoreRepositoryInterface
{
    /**
     * CustomerRepository constructor.
     *
     * @param CustomerStore $model
     */
    public function __construct(CustomerStore $model)
    {
        parent::__construct($model);
    }

    /**
     * @Override
     */
    public function getDataOptions(?Criteria $criteria = null): Collection
    {
        if (!$criteria) {
            $criteria = new Criteria();
        }
        $query = $this->newQuery()->scopes($this->loadScopes($criteria->getFiltersDropdown()));
        if (!empty($criteria->getSelect())) {
            $query->select($criteria->getSelect());
        }

        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->getRelations($criteria))
            ->withCount($this->getCountRelations($criteria))
            ->get();
    }
}
