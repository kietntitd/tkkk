<?php

namespace App\Repositories\Impl;

use App\Models\Staff;
use App\Repositories\StaffRepositoryInterface;

use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * Class SetRepository
 *
 * @package App\Repositories\Impl
 *
 * @property Set $model
 */
class StaffRepository extends BaseRepository implements StaffRepositoryInterface
{
    /**
     * SetRepository constructor.
     * 
     * @param Set $model
     * 
     */
    public function __construct(Set $model)
    {
        parent::__construct($model);
    }
}
