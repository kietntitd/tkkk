<?php

namespace App\Repositories;

/**
 * Interface SetRepositoryInterface
 *
 * @package App\Repositories
 */
interface SetRepositoryInterface extends BaseRepositoryInterface
{
}
