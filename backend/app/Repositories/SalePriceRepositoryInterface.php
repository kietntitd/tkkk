<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface SalePriceRepositoryInterface
 *
 * @package App\Repositories
 */
interface SalePriceRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check Sale Price isExits Product And Customer
     *
     * @param $productId
     * @param $customerId
     * @return mixed
     */
    public function isExitsProductAndCustomer($productId, $customerId);

    /**
     * Find model with key business
     *
     * @param $relations
     * @param $keys
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Support\Collection
     */
    public function findWithKeyBusiness($relations, $keys);

    /**
     * Get Price By Date And Product
     *
     * @param $productId
     * @param $date
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]|mixed
     */
    public function getPriceByDateAndProduct($productId, $date);
}
