<?php

namespace App\Repositories;
use App\Http\Parameters\Criteria;
use App\Models\BaseModel;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
/**
 * Interface LedgerRepositoryInterface
 *
 * @package App\Repositories
 */
interface LedgerRepositoryInterface extends BaseRepositoryInterface
{
    public function listStaff(Criteria $criteria): LengthAwarePaginator;
}
