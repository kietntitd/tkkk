<?php

namespace App\Repositories;

/**
 * Interface CustomerRepositoryInterface
 *
 * @package App\Repositories
 */
interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check the id customer exists
     *
     * @param $customerId
     * @return mixed
     */
    public function isExistsCustomer($customerId);
}
