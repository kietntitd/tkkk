<?php

namespace App\Repositories;

/**
 * Interface CustomerStoreRepositoryInterface
 *
 * @package App\Repositories
 */
interface CustomerStoreRepositoryInterface extends BaseRepositoryInterface
{
}
