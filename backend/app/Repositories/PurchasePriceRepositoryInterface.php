<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface PurchasePriceRepositoryInterface
 *
 * @package App\Repositories
 */
interface PurchasePriceRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check Purchase Price isExits Product And BusinessPartner
     *
     * @param $productId
     * @param $businessPartnerId
     * @return mixed
     */
    public function isExitsProductAndBusinessPartner($productId, $businessPartnerId);

    /**
     * Find model with key business
     *
     * @param $relations
     * @param $keys
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Support\Collection
     */
    public function findWithKeyBusiness($relations, $keys);
}
