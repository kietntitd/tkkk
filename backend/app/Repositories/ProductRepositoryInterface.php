<?php

namespace App\Repositories;

/**
 * Interface ProductRepositoryInterface
 *
 * @package App\Repositories
 */
interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check the id product exists
     *
     * @param $productId
     * @return mixed
     */
    public function isExistsProduct($productId);
}
