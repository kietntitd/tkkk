<?php

namespace App\Repositories;

/**
 * Interface CustomerDepartmentRepositoryInterface
 *
 * @package App\Repositories
 */
interface CustomerDepartmentRepositoryInterface extends BaseRepositoryInterface
{
}
