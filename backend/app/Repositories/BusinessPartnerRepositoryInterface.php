<?php

namespace App\Repositories;

/**
 * Interface BusinessPartnerRepositoryInterface
 *
 * @package App\Repositories
 */
interface BusinessPartnerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check the id business_partner exists
     *
     * @param $businessPartnerId
     * @return mixed
     */
    public function isExistsBusinessPartner($businessPartnerId);
}
