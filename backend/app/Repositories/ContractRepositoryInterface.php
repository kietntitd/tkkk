<?php

namespace App\Repositories;

/**
 * Interface ContractRepositoryInterface
 *
 * @package App\Repositories
 */
interface ContractRepositoryInterface extends BaseRepositoryInterface
{
}
