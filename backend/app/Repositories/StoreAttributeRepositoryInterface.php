<?php

namespace App\Repositories;

/**
 * Interface SetRepositoryInterface
 *
 * @package App\Repositories
 */
interface StoreAttributeRepositoryInterface extends BaseRepositoryInterface
{
}
