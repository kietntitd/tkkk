<?php

namespace App\Repositories;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories
 */
interface UserRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check unique email
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function isUniqueEmail(array $data, string $mode);
}
