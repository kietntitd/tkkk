<?php

namespace App\Repositories;

/**
 * Interface WarehouseShelfRepositoryInterface
 *
 * @package App\Repositories
 */
interface WarehouseShelfRepositoryInterface extends BaseRepositoryInterface
{
}
