<?php

namespace App\Repositories;

/**
 * Interface AdminRepositoryInterface
 *
 * @package App\Repositories
 */
interface AdminRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Check unique email
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */
    public function isUniqueEmail(array $data, string $mode);
}
