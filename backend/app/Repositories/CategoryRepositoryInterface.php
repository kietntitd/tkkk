<?php

namespace App\Repositories;

/**
 * Interface CategoryRepositoryInterface
 *
 * @package App\Repositories
 */
interface CategoryRepositoryInterface extends BaseRepositoryInterface
{
}
