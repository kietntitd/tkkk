<?php

namespace App\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface UserService extends BaseService
{
    /**
     * Find user with type
     *
     * @param $id
     * @param string|null $type
     * @return LengthAwarePaginator
     */
    public function findWithType($id, ?string $type): object;
}
