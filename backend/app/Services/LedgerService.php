<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface LedgerService extends BaseService
{
    public function listStaff(Criteria $criteria): LengthAwarePaginator;
}
