<?php

namespace App\Services;

interface ProductService extends BaseService
{
    /**
     * Check the id product exists
     *
     * @param $productId
     * @return mixed
     */
    public function isExistsProduct($productId);

    /**
     * Delete set
     *
     * @param $setId
     * @return mixed
     */
    public function deleteSet($setId);

    /**
     * Create new model
     *
     * @param $data
     * @return mixed
     */
    public function createSet($data);

    /**
     * Update model item
     *
     * @return mixed
     */
    public function updateSet($id, $data);

    /**
     * Find model(s) by id
     *
     * @param $id
     * @return object
     */
    public function findSet($id): object;
}
