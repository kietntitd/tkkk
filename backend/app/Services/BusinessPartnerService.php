<?php

namespace App\Services;

interface BusinessPartnerService extends BaseService
{
    /**
     * Check the Business Partner is exists or active
     *
     * @param $id
     * @return mixed
     */
    public function isExistBusinessPartner($id);
}
