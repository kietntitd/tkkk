<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Http\Responses\LoginResponse;
use App\Http\Responses\BaseResponse as ServiceResponse;
use App\Repositories\Impl\AdminRepository;
use App\Repositories\Impl\UserRepository;
use App\Repositories\Impl\PasswordResetRepository;
use App\Models\Admin;

use Exception;
use Carbon\Carbon;

use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class AuthenticationService
{

    public function __construct(
        private LoginResponse $loginResponse,
        private ServiceResponse $serviceResponse,
        private AdminRepository $adminRepository,
        private UserRepository $userRepository,
        private PasswordResetRepository $passwordResetRepository
    ) {
    }

    public function login($request, $guard = 'api_admin')
    {
        $credentials = $request->only('email', 'password');
        $credentials['is_active'] = Admin::ACTIVE; 
        $token = Auth::guard($guard)->attempt($credentials);
        if (!$token) {
            $this->loginResponse->setCode(Response::HTTP_UNAUTHORIZED);
            $this->loginResponse->setMessage('Unauthorized');
        } else {
            $this->loginResponse->setAccessToken($token);
            $this->loginResponse->setProfile(Auth::guard($guard)->user());
        }
        return $this->loginResponse;
    }

    public function requestPassword($request, $broker = 'admins')
    {
        $status = Password::broker($broker)->sendResetLink(
            $request->only('email')
        );
        if ($status !== Password::RESET_LINK_SENT) {
            $this->serviceResponse->setCode(Response::HTTP_BAD_REQUEST);
        }
        $this->serviceResponse->setMessage(__($status));
        return $this->serviceResponse;
    }

    public function resetPassword($request, $broker = 'admins')
    {
        $status = Password::broker($broker)->reset(
            $request->only('password', 'password_confirmation', 'token', 'email'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => $password
                ]);
                $user->save();
                event(new PasswordReset($user));
        }
        );
        if ($status !== Password::PASSWORD_RESET) {
            $this->serviceResponse->setCode(Response::HTTP_BAD_REQUEST);
        }
        $this->serviceResponse->setMessage(__($status));
        return $this->serviceResponse;
    }

    public function refreshToken($token)
    {
        if (!$token) {
            $this->loginResponse->setCode(Response::HTTP_UNAUTHORIZED);
            $this->loginResponse->setMessage(trans('errors.invalid_token'));
        }
        try {
            $token = JWTAuth::refresh();
            $this->loginResponse->setAccessToken($token);
        } catch (Exception $e) {
            $this->loginResponse->setCode(Response::HTTP_UNAUTHORIZED);
            $this->loginResponse->setMessage(trans('errors.invalid_token'));
        }
        return $this->loginResponse;
    }

    public function activateUser($request, $type = 'admin')
    {
        $email = $request->get('email');
        $token = $request->get('token');
        if ($this->validateResetToken($email, $token)) {
            switch ($type) {
                case 'admin':
                    $user = $this->adminRepository->getByEmail($email);
                    break;
                case 'user':
                    $user = $this->userRepository->getByEmail($email);
                    break;
            }
            $user->is_active = 1;
            $user->save();
        } else {
            $this->serviceResponse->setCode(Response::HTTP_BAD_REQUEST);
            $this->serviceResponse->setMessage(trans('errors.invalid_token'));
        }
        return $this->serviceResponse;
    }

    public function resendConfirmEmail($email) {
        try {
            $this->passwordResetRepository->deleteByEmail($email);
            Password::sendResetLink(['email' => $email], function ($admin, $token) {
                $admin->sendNewPasswordNotification($token);
            });
        } catch (Exception $exception) {
            $this->serviceResponse->setCode(Response::HTTP_BAD_REQUEST);
            $this->serviceResponse->setMessage(trans('errors.something_error'));
        }
        return $this->serviceResponse;
    }

    private function validateResetToken($email, $token)
    {
        $password_resets = DB::table('password_resets')
            ->where('email', $email)->first();

        if ($password_resets &&  Hash::check($token, $password_resets->token)) {
            $createdAt = Carbon::parse($password_resets->created_at);
            if (!Carbon::now()->greaterThan($createdAt->addMinutes(config('auth.passwords.users.expire')))) {
                return true;
            }
        }

        return false;
    }
}
