<?php

namespace App\Services;

interface SalePriceService extends BaseService
{
    /**
     * Find model with key business
     *
     * @param $keys
     * @return array
     */
    public function findWithKeyBusiness($keys): array;

    /**
     * Get Price By Date And Product
     *
     * @param $productId
     * @param $date
     * @return mixed
     */
    public function getPriceByDateAndProduct($productId, $date);

    /**
     * Check is exists product and customer in sale price
     *
     * @param $productId
     * @param $customerId
     * @return mixed
     */
    public function isExitsProductAndCustomer($productId, $customerId);
}
