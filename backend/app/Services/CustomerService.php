<?php

namespace App\Services;

interface CustomerService extends BaseService
{
    /**
     * Check the id customer exists and active
     *
     * @param $id
     * @return mixed
     */
    public function isExistCustomer($id);
}
