<?php

namespace App\Services;

interface PurchasePriceService extends BaseService
{
    /**
     * Find model with key business
     *
     * @param $keys
     * @return array
     */
    public function findWithKeyBusiness($keys): array;

    /**
     * Get Purchase Price By Date And Product
     *
     * @param $productId
     * @param $date
     * @return mixed
     */
    public function getPriceByDateAndProduct($productId, $date);

    /**
     * Check is exists product and business partner in sale price
     *
     * @param $productId
     * @param $businessPartnerId
     * @return mixed
     */
    public function isExitsProductAndBusinessPartner($productId, $businessPartnerId);
}
