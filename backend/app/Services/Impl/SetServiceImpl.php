<?php

namespace App\Services\Impl;

use App\Repositories\Impl\SetRepository;
use App\Repositories\Impl\SetProductRepository;
use App\Services\SetService;

use App\Http\Parameters\Criteria;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class SetServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property SetRepository $repository
 */
class SetServiceImpl extends BaseServiceImpl implements SetService
{

    /** @var SetProductRepository $setProductRepo */
    public SetProductRepository $setProductRepo;

    /**
     * SetServiceImpl constructor.
     * @param SetRepository $repository
     */
    public function __construct(
        SetRepository $repository,
        SetProductRepository $setProductRepo,
    ) {
        parent::__construct($repository);
        $this->setProductRepo = $setProductRepo;
    }

    /**
     * Get list model items by pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        return $this->repository->list($criteria->setRelations(['products', 'customer']));
    }

    /**
     * Create new model item
     *
     * @param array $data
     *
     * @return Model
     *
     * @throws Exception
     */
    public function create(array $data): Model
    {
        $set = $this->repository->create($data);
        $productList = $data['products'];
        $this->setProductRepo->saveMany($set->id, $productList);
        return $this->repository->findWithRelations($set->id, ['products', 'customer']);
    }


    /**
     * Update model item data by hash id
     *
     * @param string $id
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function update($id, array $data): Model
    {
        $set = $this->repository->find($id);
        $productList = $data['products'];
        $this->repository->update($id, $data);
        $this->setProductRepo->updateMany($set, $productList);
        return $this->repository->findWithRelations($id, ['products', 'customer']);
    }

    /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['setProducts', 'products', 'customer']);
    }
}
