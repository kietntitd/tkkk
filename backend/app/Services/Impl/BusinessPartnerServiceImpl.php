<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\Impl\BusinessPartnerRepository;
use App\Repositories\Impl\BusinessPartnerRelRepository;
use App\Services\BusinessPartnerService;
use App\Utils\MessageCommon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class BusinessPartnerServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property BusinessPartnerRepository $repository
 *
 */
class BusinessPartnerServiceImpl extends BaseServiceImpl implements BusinessPartnerService
{
    public BusinessPartnerRelRepository $businessPartnerRelRepository;

    /**
     * CustomerServiceImpl constructor.
     *
     * @param BusinessPartnerRepository $repository
     * @param BusinessPartnerRelRepository $businessPartnerRelRepository
     */
    public function __construct(
        BusinessPartnerRepository $repository
        , BusinessPartnerRelRepository $businessPartnerRelRepository,
    )
    {
        parent::__construct($repository);
        $this->businessPartnerRelRepository = $businessPartnerRelRepository;
    }

    /**
     * Get list model items by pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        if (isset($criteria->getFilters()['idRemove'])) {
            return $this->repository->disableSelfRecord($criteria->getFilters()['idRemove'])->paginate($criteria->getLimit());
        }
        return $this->repository->list($criteria);
    }

    /**
     * Create new model item
     *
     * @param array $data
     *
     * @return Model
     *
     * @throws Exception
     */
    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();
            $businessPartner = $this->repository->create($data);
            $factoryList = $data['businessPartnersRel'];
            $this->businessPartnerRelRepository->saveMany($businessPartner->id, $factoryList);
            DB::commit();
            return $this->repository->findWithRelations($businessPartner->id, ['businessPartnersRels']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception(MessageCommon::MS02_004);
        }
    }

    /**
     * Update model item data by hash id
     *
     * @param string $id
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function update($id, array $data): Model
    {
        try {
            DB::beginTransaction();
            $businessPartner = $this->repository->find($id);
            $businessPartner->update($data);
            $factoryList = $data['businessPartnersRel'];
            $this->businessPartnerRelRepository->updateMany($businessPartner, $factoryList);
            DB::commit();
            return $this->repository->findWithRelations($id, ['businessPartnersRels']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception(MessageCommon::MS02_005);
        }
    }

    /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['children']);
    }

    /**
     * @inheritDoc
     */
    public function isExistBusinessPartner($id)
    {
        return $this->repository->isExistsBusinessPartner($id);
    }
}
