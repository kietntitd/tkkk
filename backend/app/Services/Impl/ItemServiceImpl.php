<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\Impl\ItemRepository;
use App\Services\ItemService;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ItemManagementServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property ItemRepository $repository
 */
class ItemServiceImpl extends BaseServiceImpl implements ItemService
{
    /**
     * ItemManagementServiceImpl constructor.
     *
     * @param ItemRepository $repository
     */
    public function __construct(
        ItemRepository $repository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['product']);
        return parent::list($criteria);
    }

     /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['product']);
    }
}
