<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Models\User;
use App\Repositories\Impl\UserRepository;
use App\Services\AdminService;
use App\Services\UserService;
use App\Utils\MessageCommon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FavoriteServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property UserRepository $repository
 */
class UserServiceImpl extends BaseServiceImpl implements UserService
{

    /**
     * UserServiceImpl constructor.
     * @param UserRepository $repository
     * @param AdminService $adminService
     */
    public function __construct(
        UserRepository      $repository,
        public AdminService $adminService
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        if ($criteria->getFilters(['type'])['type'] == USER::TYPE_WAREHOUSE) {
            $criteria->setRelations(['customer', 'businessPartner']);
            return $this->adminService->list($criteria);
        }
        $criteria->setRelations(['customer', 'businessPartner']);
        return parent::list($criteria);
    }

    /**
     * @Override
     */
    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();
            if ($data['type'] === USER::TYPE_WAREHOUSE) {
                $user = $this->adminService->create($data);
            } else {
                $this->isUniqueEmail($data, 'create');
                $user = parent::create($data);
            }
            Password::sendResetLink(['email' => $user->email], function ($user, $token) {
                $user->sendNewPasswordNotification($token);
            });
            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollBack();
            if ($e->getCode() === Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE) {
                throw new ModelNotFoundException($e->getMessage());
            }
            throw new Exception(MessageCommon::MS02_004);
        }
    }

    /**
     * @Override
     */
    public function update($id, array $data): Model
    {
        try {
            DB::beginTransaction();
            if ($data['type'] === USER::TYPE_WAREHOUSE) {
                $user = $this->adminService->update($id, $data);
            } else {
                $this->isUniqueEmail($data, 'update', $id);
                $user = parent::update($id, $data);
            }
            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollBack();
            if ($e->getCode() === Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE) {
                throw new ModelNotFoundException($e->getMessage());
            }
            throw new Exception(MessageCommon::MS02_004);
        }
    }

    private function isUniqueEmail(array $data, string $mode)
    {
        if ($this->repository->isUniqueEmail($data, $mode)) {
            throw new ModelNotFoundException(
                __('validation.unique', ['attribute' => __('field.users.email')]),
                Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function findWithType($id, ?string $type): object
    {
        if ($type === USER::TYPE_WAREHOUSE) {
            return $this->adminService->find($id);
        }
        return $this->repository->find($id);
    }
}
