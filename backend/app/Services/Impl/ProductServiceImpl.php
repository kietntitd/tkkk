<?php

namespace App\Services\Impl;

use App\Exceptions\DeleteProductFailedException;
use App\Http\Parameters\Criteria;
use App\Repositories\Impl\BarCodeRepository;
use App\Repositories\Impl\CategoryRepository;
use App\Repositories\Impl\ContractSetRepository;
use App\Repositories\Impl\ProductRepository;
use App\Repositories\Impl\PurchasePriceRepository;
use App\Repositories\Impl\SalePriceRepository;
use App\Repositories\Impl\SetProductRepository;
use App\Services\ProductService;
use App\Utils\MessageCommon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property ProductRepository $repository
 */
class ProductServiceImpl extends BaseServiceImpl implements ProductService
{
    /**
     * ProductServiceImpl constructor.
     * @param ProductRepository $repository
     * @param SalePriceRepository $salePriceRepository
     * @param SetProductRepository $setProductRepository
     * @param BarCodeRepository $barCodeRepository
     * @param PurchasePriceRepository $purchasePriceRepository
     * @param ContractSetRepository $contractSetRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        ProductRepository           $repository,
        public SalePriceRepository  $salePriceRepository,
        public SetProductRepository $setProductRepository,
        public BarCodeRepository $barCodeRepository,
        public PurchasePriceRepository $purchasePriceRepository,
        public ContractSetRepository $contractSetRepository,
        public CategoryRepository $categoryRepository
    )
    {
        parent::__construct($repository);
    }


    /**
     * Get list model items by pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        return $this->repository->list($criteria->setRelations(['category:id,name', 'businessPartner:id,name', 'salePrices']));
    }

    /**
     * Get list sets model items by pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function listSets(Criteria $criteria): LengthAwarePaginator
    {
        return $this->repository->list($criteria->setRelations(['customer:id,name', 'setProduct']));
    }

    /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['businessPartner', 'salePrices']);
    }

    /**
     * Delete model item by id
     *
     * @param string $id
     *
     * @throws ModelNotFoundException|Exception
     */
    public function delete($id): void
    {
        $product = $this->repository->find($id);
        if ($this->setProductRepository->isExistInSet($product->id)) {
            throw new DeleteProductFailedException(trans('exception.product.delete_set'));
        }
        if (count($product->salePrices) > 0) {
            throw new DeleteProductFailedException(trans('exception.product.delete_priced'));
        }
        if ($this->salePriceRepository->isExistProduct($product->id)) {
            throw new DeleteProductFailedException(trans('exception.product.delete_quantity_greater_zero'));
        }
        $this->repository->delete($id);
    }

    /**
     * @inheritDoc
     */
    public function isExistsProduct($productId)
    {
        return $this->repository->isExistsProduct($productId);
    }

    /**
     * @inheritDoc
     */
    public function deleteSet($id): void
    {
        if ($this->purchasePriceRepository->isExistProduct($id) || $this->salePriceRepository->isExistProduct($id)) {
            throw new DeleteProductFailedException(trans('exception.product.delete_set_have_price'));
        }
        if ($this->barCodeRepository->isUsingSet($id) || $this->contractSetRepository->isUsingSet($id)) {
            throw new DeleteProductFailedException(trans('exception.product.delete_set_using'));
        }
        try {
            DB::beginTransaction();
            $this->setProductRepository->deleteBySetId($id);
            parent::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * @inheritDoc
     */
    public function createSet($data)
    {
        foreach ($data['set_product'] as $product) {
            if (!$this->isExistsProduct($product['product_id'])) {
                throw new ModelNotFoundException(
                    __('validation.exists', ['attribute' => __('field.sets.product')]),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        }
        try {
            DB::beginTransaction();
            $set = $this->repository->create($data);
            $this->setProductRepository->saveMany($set['id'], $data['set_product']);
            DB::commit();
            return $set;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new Exception($exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function updateSet($id, $data)
    {
        if ($this->barCodeRepository->isUsingSet($id) || $this->contractSetRepository->isUsingSet($id)) {
            throw new ModelNotFoundException(trans('exception.product.delete_set_using'));
        }
        foreach ($data['set_product'] as $product) {
            if (!$this->isExistsProduct($product['product_id'])) {
                throw new ModelNotFoundException(
                    __('validation.exists', ['attribute' => __('field.sets.product')]),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        }
        try {
            DB::beginTransaction();
            $set = $this->findSet($id);
            $setList = $data['set_product'];
            $dataSet = $this->repository->update($id, $data);
            $this->setProductRepository->updateMany($set, $setList);
            DB::commit();
            return $dataSet;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new Exception($exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function findSet($id): object
    {
        return $this->repository->findWithRelations($id, ['customer:id,name', 'setProduct.products']);
    }
    /**
     * Create new model item
     *
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function create(array $data): Model
    {
        if (!$this->categoryRepository->isExistsCategory($data['category_id'])) {
            throw new ModelNotFoundException(__('validation.exists', ['attribute' => __('field.products.category_id')]),422);
        }
        return $this->repository->create($data);
    }

    /**
     * Update model item data by hash id
     *
     * @param string $id
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function update($id, array $data): Model
    {
        if (!$this->categoryRepository->isExistsCategory($data['category_id'])) {
            throw new ModelNotFoundException(__('validation.exists', ['attribute' => __('field.products.category_id')]),422);
        }
        return $this->repository->update($id, $data);
    }
}
