<?php

namespace App\Services\Impl;

use App\Repositories\Impl\UserCustomerRepository;
use App\Services\UserCustomerService;

/**
 * Class FavoriteServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property UserCustomerRepository $repository
 */
class UserCustomerServiceImpl extends BaseServiceImpl implements UserCustomerService
{

    /**
     * FavoriteServiceImpl constructor.
     * @param UserCustomerRepository $repository
     */
    public function __construct(
        UserCustomerRepository $repository
    )
    {
        parent::__construct($repository);
    }

}
