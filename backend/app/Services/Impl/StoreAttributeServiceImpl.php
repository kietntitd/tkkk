<?php

namespace App\Services\Impl;

use App\Repositories\Impl\StoreAttributeRepository;
use App\Services\StoreAttributeService;

/**
 * Class AdminServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property StoreAttributeRepository $repository
 */
class StoreAttributeServiceImpl extends BaseServiceImpl implements StoreAttributeService
{
    /**
     * StoreAttributeServiceImpl constructor.
     *
     * @param StoreAttributeRepository $repository
     */
    public function __construct(
        StoreAttributeRepository $repository
    )
    {
        parent::__construct($repository);
    }
}
