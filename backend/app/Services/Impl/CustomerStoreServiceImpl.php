<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\BusinessPartnerRepositoryInterface;
use App\Repositories\CustomerRepositoryInterface;
use App\Repositories\CustomerStoreRepositoryInterface;
use App\Services\CustomerStoreService;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomerStoreServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property CustomerStoreRepositoryInterface $repository
 */
class CustomerStoreServiceImpl extends BaseServiceImpl implements CustomerStoreService
{
    /**
     * CustomerStoreServiceImpl constructor.
     *
     * @param CustomerStoreRepositoryInterface $repository
     * @param CustomerRepositoryInterface $customerRepository
     * @param BusinessPartnerRepositoryInterface $businessPartnerRepository
     */
    public function __construct(
        CustomerStoreRepositoryInterface          $repository,
        public CustomerRepositoryInterface        $customerRepository,
        public BusinessPartnerRepositoryInterface $businessPartnerRepository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @Override
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(
            [
                'customer',
                'businessPartner',
                'storeAttributeWithAreaId',
                'storeAttributeWithJobId',
                'storeAttributeWithJobTypeId',
            ]
        );

        return parent::list($criteria);
    }

    /**
     * @Override
     */
    public function find($id): object
    {
        $dataModel = $this->repository->findWithRelations($id, ['customer', 'businessPartner']);
        $dataModel['pickup_circle'] = json_decode($dataModel['pickup_circle']);
        return $dataModel;
    }

    /**
     * @Override
     */
    public function create(array $data): Model
    {
        $data['pickup_circle'] = json_encode($data['pickup_circle']);
        if (!$this->customerRepository->isExistsCustomer($data['customer_id'])) {
            throw new ModelNotFoundException(
                __('validation.exists', ['attribute' => __('field.customer_stores.customer_name')]),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        if (!$this->businessPartnerRepository->isExistsBusinessPartner($data['business_partner_id'])) {
            throw new ModelNotFoundException(
                __('validation.exists', ['attribute' => __('field.customer_stores.business_partner_id')]),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        return parent::create($data);
    }

    /**
     * @Override
     */
    public function update($id, array $data): Model
    {
        if (!$this->customerRepository->isExistsCustomer($data['customer_id'])) {
            throw new ModelNotFoundException(
                __('validation.exists', ['attribute' => __('field.customer_stores.customer_name')])
            );
        }
        if (!$this->businessPartnerRepository->isExistsBusinessPartner($data['business_partner_id'])) {
            throw new ModelNotFoundException(
                __('validation.exists', ['attribute' => __('field.customer_stores.business_partner_id')])
            );
        }
        return parent::update($id, $data);
    }
}
