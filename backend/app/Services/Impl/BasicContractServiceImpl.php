<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\Impl\BasicContractRepository;
use App\Repositories\Impl\ContractRepository;
use App\Services\BasicContractService;
use App\Utils\MessageCommon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BasicContractServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property BasicContractRepository $repository
 */
class BasicContractServiceImpl extends BaseServiceImpl implements BasicContractService
{
    /**
     * BasicContractRepository constructor.
     *
     * @param BasicContractRepository $repository
     * @param ContractRepository $contractRepository
     */
    public function __construct(
        BasicContractRepository   $repository,
        public ContractRepository $contractRepository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @Override
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['customer']);
        return parent::list($criteria);
    }

    /**
     * @Override
     */
    public function delete($id): void
    {
        try {
            DB::beginTransaction();
            $this->contractRepository->deleteByBasicContractId($id);
            parent::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * @Override
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations(
            $id,
            [
                'customer',
                'contracts.stores',
                'contracts.contractSet',
                'contracts.contractSet.product:id,name,price,factory_product_code',
            ]
        );
    }

    /**
     * @Override
     * @throws \Exception
     */
    public function update($id, array $data): Model
    {
            if ($data['is_active'] === 0 && $this->hasUsingContract($data['contracts'])) {
                // TODO update message, request SE
                throw new ModelNotFoundException('Không xóa được!');
            }
            return parent::update($id, $data);
    }

    /**
     * @Override
     */
    public function hasUsingContract($listContract): bool
    {
        foreach ($listContract as $contract) {
            if ($this->contractRepository->hasUsingContract($contract['id'])) {
                return true;
            }
        }
        return false;
    }
}
