<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\Impl\CustomerRepository;
use App\Services\CustomerDepartmentService;
use App\Repositories\Impl\CustomerDepartmentRepository;
use App\Utils\MessageCommon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class CustomerDepartmentServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property CustomerDepartmentRepository $repository
 */
class CustomerDepartmentServiceImpl extends BaseServiceImpl implements CustomerDepartmentService
{
    /**
     * CustomerDepartmentServiceImpl constructor.
     *
     * @param CustomerDepartmentRepository $repository
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        CustomerDepartmentRepository $repository,
        public CustomerRepository $customerRepository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['customer','customerStore']);
        return parent::list($criteria);
    }

     /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['customer','customerStore']);
    }

    /**
     * Delete model item by hash id
     *
     * @param string $id
     *
     * @throws ModelNotFoundException|Exception
     */
    public function delete($id): void
    {
        if ($this->repository->isExistsStaffInDepartment($id)) {
            throw new ModelNotFoundException(MessageCommon::MS03_043,422);
        }
        $this->repository->delete($id);
    }

    /**
     * Create new model item
     *
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function create(array $data): Model
    {
        if(!$this->customerRepository->isExistsCustomer($data['customer_id'])){
            throw new ModelNotFoundException(__('validation.exists', ['attribute' => __('field.customer_departments.customer_name')]),422);
        }
        return $this->repository->create($data);
    }

    /**
     * Update model item data by hash id
     *
     * @param string $id
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|Exception
     */
    public function update($id, array $data): Model
    {
        if(!$this->customerRepository->isExistsCustomer($data['customer_id'])){
            throw new ModelNotFoundException(__('validation.exists', ['attribute' => __('field.customer_departments.customer_name')]),422);
        }
        return $this->repository->update($id, $data);
    }
}
