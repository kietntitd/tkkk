<?php

namespace App\Services\Impl;

use App\Repositories\Impl\WarehouseShelfRepository;
use App\Services\WarehouseShelfService;

/**
 * Class WarehouseShelfServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property WarehouseShelfRepository $repository
 */
class WarehouseShelfServiceImpl extends BaseServiceImpl implements WarehouseShelfService
{
    /**
     * WhShelfRepository constructor.
     * @param WarehouseShelfRepository $repository
     */
    public function __construct(
        WarehouseShelfRepository $repository
    )
    {
        parent::__construct($repository);
    }
}
