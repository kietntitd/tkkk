<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Models\PurchasePrice;
use App\Repositories\Impl\PurchasePriceRepository;
use App\Services\PurchasePriceService;
use Helper\Common;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class PurchasePriceServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property PurchasePriceRepository $repository
 */
class PurchasePriceServiceImpl extends BaseServiceImpl implements PurchasePriceService
{
    /**
     * PurchasePriceServiceImpl constructor.
     *
     * @param PurchasePriceRepository $repository
     */
    public function __construct(
        PurchasePriceRepository $repository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['businessPartner', 'product']);
        return parent::list($criteria);
    }


    /**
     * @inheritDoc
     */
    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();
            $purchasePriceChildrenModel = new PurchasePrice();
            if ($data['products']) {
                $products = $data['products'];
                for ($i = 0; $i < count($products); $i++) {
                    $purchasePriceParentData = [
                        'business_partner_id' => $data['business_partner_id'],
                        'product_id' => $products[$i]['id'],
                    ];
                    $purchasePriceParentModel = $this->repository->create($purchasePriceParentData);
                    $purchasePriceChildren = [
                        'business_partner_id' => $data['business_partner_id'],
                        'product_id' => $products[$i]['id'],
                        'wash_price' => $products[$i]['wash_price'],
                        'start_date' => $products[$i]['start_date'],
                        'end_date' => $products[$i]['end_date'] ?? null,
                        'parent_id' => $purchasePriceParentModel->id,
                    ];
                    $purchasePriceChildrenModel = $this->repository->create($purchasePriceChildren);
                }
            }
            DB::commit();
            return $purchasePriceChildrenModel;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception($exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function findWithKeyBusiness($keys): array
    {
        $data = [];
        $purchasePrices = $this->repository->findWithKeyBusiness(['businessPartner', 'product'], $keys);
        foreach ($purchasePrices as $purchasePrice) {
            if (!$purchasePrice->parent_id) {
                $data['parent'] = $purchasePrice;
            } else {
                if ($purchasePrice->start_date) {
                    $purchasePrice->start_date = Common::formatDate($purchasePrice->start_date, "Y-m-d");
                }
                if ($purchasePrice->end_date) {
                    $purchasePrice->end_date = Common::formatDate($purchasePrice->end_date, "Y-m-d");
                }
                $data['children'][] = $purchasePrice;
            }
        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getPriceByDateAndProduct($productId, $date)
    {
        return $this->repository->getPriceByDateAndProduct($productId, $date);
    }

    /**
     * @inheritDoc
     */
    public function isExitsProductAndBusinessPartner($productId, $businessPartnerId)
    {
        return $this->repository->isExitsProductAndBusinessPartner($productId, $businessPartnerId);
    }

    /**
     * @inheritDoc
     */
    public function update($id, array $data): Model
    {
        try {
            DB::beginTransaction();
            $purchasePriceChildrenModel = new PurchasePrice();
            foreach ($data['deletedTransactions'] as $deletedTransaction) {
                $this->repository->delete($deletedTransaction);
            }
            if ($data['children']) {
                $children = $data['children'];
                foreach ($children as $child) {
                    if ($child['id']) {
                        $purchasePriceChildrenModel = $this->repository->update($child['id'], $child);
                    } else {
                        $child['business_partner_id'] = $data['business_partner_id'];
                        $child['product_id'] = $data['product_id'];
                        $child['parent_id'] = $data['parent']['id'];
                        $purchasePriceChildrenModel = $this->repository->create($child);
                    }
                }
            }
            DB::commit();
            return $purchasePriceChildrenModel;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception($exception);
        }
    }

}
