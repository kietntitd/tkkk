<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Models\SalePrice;
use App\Repositories\Impl\SalePriceRepository;
use App\Services\SalePriceService;
use App\Utils\MessageCommon;
use Exception;
use Helper\Common;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SalePriceServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property SalePriceRepository $repository
 */
class SalePriceServiceImpl extends BaseServiceImpl implements SalePriceService
{
    /**
     * SalePriceServiceImpl constructor.
     *
     * @param SalePriceRepository $repository
     */
    public function __construct(
        SalePriceRepository $repository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['customer', 'product']);
        return parent::list($criteria);
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();
            $salePriceChildrenModel = new SalePrice();
            if ($data['products']) {
                $products = $data['products'];
                for ($i = 0; $i < count($products); $i++) {
                    $salePriceParentData = [
                        'customer_id' => $data['customer_id'],
                        'product_id' => $products[$i]['id'],
                    ];
                    $salePriceParentModel = $this->repository->create($salePriceParentData);
                    $salePriceChildren = [
                        'customer_id' => $data['customer_id'],
                        'product_id' => $products[$i]['id'],
                        'rent_price' => $products[$i]['rent_price'],
                        'sell_price' => $products[$i]['sell_price'],
                        'wash_price' => $products[$i]['wash_price'],
                        'start_date' => $products[$i]['start_date'],
                        'end_date' => $products[$i]['end_date'] ?? null,
                        'parent_id' => $salePriceParentModel->id,
                    ];
                    $salePriceChildrenModel = $this->repository->create($salePriceChildren);
                }
            }
            DB::commit();
            return $salePriceChildrenModel;
        } catch (Exception $exception) {
            DB::rollBack();
            throw new Exception($exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function findWithKeyBusiness($keys): array
    {
        $data = [];
        $salePrices = $this->repository->findWithKeyBusiness(['customer', 'product'], $keys);
        foreach ($salePrices as $salePrice) {
            if (!$salePrice->parent_id) {
                $data['parent'] = $salePrice;
            } else {
                if ($salePrice->start_date) {
                    $salePrice->start_date = Common::formatDate($salePrice->start_date, "Y-m-d");
                }
                if ($salePrice->end_date) {
                    $salePrice->end_date = Common::formatDate($salePrice->end_date, "Y-m-d");
                }
                $data['children'][] = $salePrice;
            }
        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function update($id, array $data): Model
    {
        try {
            DB::beginTransaction();
            $salePriceChildrenModel = new SalePrice();
            foreach ($data['deletedTransactions'] as $deletedTransaction) {
                $this->repository->delete($deletedTransaction);
            }
            if ($data['children']) {
                $children = $data['children'];
                foreach ($children as $child) {
                    if ($child['id']) {
                        $salePriceChildrenModel = $this->repository->update($child['id'], $child);
                    } else {
                        $child['customer_id'] = $data['customer_id'];
                        $child['product_id'] = $data['product_id'];
                        $child['parent_id'] = $data['parent']['id'];
                        $salePriceChildrenModel = $this->repository->create($child);
                    }
                }
            }
            DB::commit();
            return $salePriceChildrenModel;
        } catch (Exception $exception) {
            DB::rollBack();
            throw new Exception($exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function getPriceByDateAndProduct($productId, $date)
    {
        return $this->repository->getPriceByDateAndProduct($productId, $date);
    }

    /**
     * @inheritDoc
     */
    public function isExitsProductAndCustomer($productId, $customerId)
    {
        return $this->repository->isExitsProductAndCustomer($productId, $customerId);
    }
}
