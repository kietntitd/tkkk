<?php

namespace App\Services\Impl;

use App\Repositories\Impl\AdminRepository;
use App\Services\AdminService;
use App\Utils\MessageCommon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Http\Parameters\Criteria;

/**
 * Class AdminServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property AdminRepository $repository
 */
class AdminServiceImpl extends BaseServiceImpl implements AdminService
{
    /**
     * AdminServiceImpl constructor.
     *
     * @param AdminRepository $repository
     */
    public function __construct(
        AdminRepository $repository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @Override
     */
    public function create(array $data): Model
    {
        $this->isUniqueEmail($data, 'create');
        try{
            DB::beginTransaction();
            $admin = parent::create($data);
            Password::sendResetLink(['email' => $admin->email], function ($admin, $token) {
                $admin->sendNewPasswordNotification($token);
            });
            DB::commit();
            return $admin;
        } catch (\Exception $e) {
            DB::rollBack();
            if ($e->getCode() === Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE) {
                throw new ModelNotFoundException($e->getMessage());
            }
            throw new \Exception(MessageCommon::MS02_004);
        }
    }

    /**
     * @Override
     */
    public function update($id, array $data): Model
    {
        $this->isUniqueEmail($data, 'edit');
        return parent::update($id, $data);
    }

    /**
     * Check email unique
     *
     * @param array $data
     * @param string $mode
     * @return void
     */
    public function isUniqueEmail(array $data, string $mode)
    {
        if ($this->repository->isUniqueEmail($data, $mode)){
            throw new ModelNotFoundException(
                __('validation.unique', ['attribute' => __('field.users.email')]),
                Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE
            );
        }
    }

    /**
     * @inheritDoc
     */

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(['customer','businessPartner']);
        return parent::list($criteria);
    }

    /**
     * Find model(s) by id
     *
     * @param int|string $id
     *
     * @return object
     * @throws Exception
     */
    public function find($id): object
    {
        $model = $this->repository->findWithRelations($id, ['customer','businessPartner']);
        if ($model) {
            $model->isset_password = !!$model->password;
        }
        return $model;
    }

}
