<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\BusinessPartnerRepositoryInterface;
use App\Repositories\CustomerRepositoryInterface;
use App\Repositories\LedgerRepositoryInterface;
use App\Services\LedgerService;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LedgerServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property LedgerRepositoryInterface $repository
 */
class LedgerServiceImpl extends BaseServiceImpl implements LedgerService
{
    /**
     * LedgerServiceImpl constructor.
     *
     * @param LedgerRepositoryInterface $repository
     * @param CustomerRepositoryInterface $customerRepository
     * @param BusinessPartnerRepositoryInterface $businessPartnerRepository
     */
    public function __construct(
        LedgerRepositoryInterface          $repository,
        public CustomerRepositoryInterface        $customerRepository,
        public BusinessPartnerRepositoryInterface $businessPartnerRepository
    )
    {
        parent::__construct($repository);
    }

    /**
     * @Override
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $criteria->setRelations(
            [
                'customer',
                'contract',
                'storeAttributeWithAreaId',
                'storeAttributeWithJobId',
                'storeAttributeWithJobTypeId',
            ]
        );

        return parent::list($criteria);
    }

    public function find($id): object
    {
        return $this->repository->findWithRelations($id, ['customer.staff']);
    }


    public function listStaff(Criteria $criteria): LengthAwarePaginator
    {
        return $this->repository->listStaff($criteria);
    }
    // /**
    //  * @Override
    //  */
    // public function find($id): object
    // {
    //     $dataModel = $this->repository->findWithRelations($id, ['customer', 'businessPartner']);
    //     $dataModel['pickup_circle'] = json_decode($dataModel['pickup_circle']);
    //     return $dataModel;
    // }

    // /**
    //  * @Override
    //  */
    // public function create(array $data): Model
    // {
    //     $data['pickup_circle'] = json_encode($data['pickup_circle']);
    //     if (!$this->customerRepository->isExistsCustomer($data['customer_id'])) {
    //         throw new ModelNotFoundException(
    //             __('validation.exists', ['attribute' => __('field.customer_stores.customer_name')]),
    //             Response::HTTP_UNPROCESSABLE_ENTITY
    //         );
    //     }
    //     if (!$this->businessPartnerRepository->isExistsBusinessPartner($data['business_partner_id'])) {
    //         throw new ModelNotFoundException(
    //             __('validation.exists', ['attribute' => __('field.customer_stores.business_partner_id')]),
    //             Response::HTTP_UNPROCESSABLE_ENTITY
    //         );
    //     }
    //     return parent::create($data);
    // }

    // /**
    //  * @Override
    //  */
    // public function update($id, array $data): Model
    // {
    //     if (!$this->customerRepository->isExistsCustomer($data['customer_id'])) {
    //         throw new ModelNotFoundException(
    //             __('validation.exists', ['attribute' => __('field.customer_stores.customer_name')])
    //         );
    //     }
    //     if (!$this->businessPartnerRepository->isExistsBusinessPartner($data['business_partner_id'])) {
    //         throw new ModelNotFoundException(
    //             __('validation.exists', ['attribute' => __('field.customer_stores.business_partner_id')])
    //         );
    //     }
    //     return parent::update($id, $data);
    // }
}
