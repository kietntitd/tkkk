<?php

namespace App\Services\Impl;

use App\Repositories\Impl\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FavoriteServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property CategoryRepository $repository
 */
class CategoryServiceImpl extends BaseServiceImpl implements CategoryService
{
    /**
     * FavoriteServiceImpl constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->uploadPath = 'categories';
        parent::__construct($repository);
    }

    public function update($id, array $data): Model
    {
        if (!empty($data['illustration_image']) && is_file($data['illustration_image'])) {
            $data['illustration_path'] = $this->uploadFile($data['illustration_image']);
        }
        return parent::update($id, $data);
    }

    public function create(array $data): Model
    {
        if (!empty($data['illustration_image'])) {
            $data['illustration_path'] = $this->uploadFile($data['illustration_image']);
        }else{
            $data['illustration_path'] = '';
        }
        return parent::create($data);
    }
}
