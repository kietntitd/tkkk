<?php

namespace App\Services\Impl;

use App\Repositories\ContractSetRepositoryInterface;
use App\Services\ContractSetService;

class ContractSetServiceImpl extends BaseServiceImpl implements ContractSetService
{
    /**
     * ContractSetServiceImpl constructor.
     *
     * @param ContractSetRepositoryInterface $repository
     */
    public function __construct(
        ContractSetRepositoryInterface $repository
    )
    {
        parent::__construct($repository);
    }
}
