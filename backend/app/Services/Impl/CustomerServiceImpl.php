<?php

namespace App\Services\Impl;

use App\Repositories\Impl\CustomerRepository;
use App\Services\CustomerService;
use Exception;

/**
 * Class CustomerServiceImpl
 *
 * @package App\Services\Impl
 *
 * @property CustomerRepository $repository
 */
class CustomerServiceImpl extends BaseServiceImpl implements CustomerService
{
    /**
     * CustomerServiceImpl constructor.
     *
     * @param CustomerRepository $repository
     */
    public function __construct(
        CustomerRepository $repository,
    )
    {
        parent::__construct($repository);
    }

    /**
     * @inheritDoc
     */
    public function isExistCustomer($id)
    {
        return $this->repository->isExistsCustomer($id);
    }

    /**
     * Delete model item by hash id
     *
     * @param string $id
     *
     * @throws Exception
     */
    public function delete($id): void
    {
        $customerName = $this->repository->find($id)->name;
        if ($this->repository->isExistsCustomerContract($id)) {
            abort(422,str_replace(
                '{0}',
                $customerName,
                '{0}は既に有効な基本契約を持っています。') // MS03_017
            );
        }
        $this->repository->delete($id);
    }
}
