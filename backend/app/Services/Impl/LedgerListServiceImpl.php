<?php

namespace App\Services\Impl;

use App\Http\Parameters\Criteria;
use App\Repositories\Impl\BusinessPartnerRepository;
use App\Repositories\Impl\BusinessPartnerRelRepository;
use App\Repositories\Impl\ItemRepository;
use App\Repositories\Impl\CustomerDepartmentRepository;
use App\Repositories\Impl\ProductRepository;
use App\Services\BusinessPartnerService;
use App\Utils\MessageCommon;
use App\Services\LedgerListService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LedgerListServiceImpl extends BaseServiceImpl implements LedgerListService
{
    public BusinessPartnerRelRepository $businessPartnerRelRepository;
    public ItemRepository $itemRepository;
    public CustomerDepartmentRepository $customerDepartmentRepository;
    public ProductRepository $productRepository;

    public function __construct(
        BusinessPartnerRepository $repository,
        BusinessPartnerRelRepository $businessPartnerRelRepository,
        CustomerDepartmentRepository $customerDepartmentRepository,
        ProductRepository $productRepository,
        ItemRepository $itemRepository,
    )
    {
        parent::__construct($repository);
        $this->businessPartnerRelRepository = $businessPartnerRelRepository;
        $this->customerDepartmentRepository = $customerDepartmentRepository;
        $this->productRepository = $productRepository;
        $this->itemRepository = $itemRepository;
    }

    /**
     * Get list model items by pagination
     *
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        // Log::info($criteria->getPage());
        // Log::info($criteria->getLimit());
        $list_item = $this->itemRepository->all();
        $list_product= $this->productRepository->all();
        $list_customer_Department= $this->productRepository->all();

        $result = DB::table('items')
                ->join('m_products', 'items.product_id', '=', 'm_products.id')
                // ->select('items.name', 'm_products.code')
                ->where('m_products.type', '=', 'set')
                ->get();
        
                Log::info($result);

        return $this->repository->list($criteria);
    }

}
