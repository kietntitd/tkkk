<?php

namespace App\Services\Impl;

use App\Repositories\Impl\BarCodeRepository;
use App\Services\BarCodeService;


class BarCodeServiceImpl extends BaseServiceImpl implements BarCodeService
{
    /**
     * BarCodeServiceImpl constructor.
     *
     * @param BarCodeRepository $repository
     */
    public function __construct(
        BarCodeRepository $repository,
    )
    {
        parent::__construct($repository);
    }
}
