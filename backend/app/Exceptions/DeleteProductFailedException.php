<?php

namespace App\Exceptions;

use Exception;

class DeleteProductFailedException extends Exception
{

  private $status = 422;

  protected $message = '';

  /**
     * Create a new exception instance.
     *
     * @param  string  $errorMessage
     * @return void
     */
    public function __construct($message = '')
    {
      $this->message = $message;
    }

    public function getStatusCode()
    {
      return $this->status;
    }
}
