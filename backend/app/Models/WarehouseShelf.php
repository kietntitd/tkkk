<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class WarehouseShelf extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_shelves';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'columns',
        'rows',
    ];

    /**
     * Filter by code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeCode(Builder $query, $code): Builder
    {
        return $query->where('code', 'like', '%' . $code . '%');
    }

    /**
     * Filter by rows min
     *
     * @param Builder $query
     * @param $min
     * @return Builder
     */
    public function scopeRowsMin(Builder $query, $min): Builder
    {
        return $query->where('rows', '>=', $min);
    }

    /**
     * Filter by rows max
     *
     * @param Builder $query
     * @param $max
     * @return Builder
     */
    public function scopeRowsMax(Builder $query, $max): Builder
    {
        return $query->where('rows', '<=', $max);
    }

    /**
     * Filter by columns min
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeColumnsMin(Builder $query, $min): Builder
    {
        return $query->where('columns', '>=', $min);
    }

    /**
     * Filter by columns max
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeColumnsMax(Builder $query, $max): Builder
    {
        return $query->where('columns', '<=', $max);
    }

    /**
     * Order by code
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByCode(Builder $query, $direction)
    {
        $query->orderBy('code', $direction);
    }

    /**
     * Order by rows
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByRows(Builder $query, $direction)
    {
        $query->orderBy('rows', $direction);
    }

    /**
     * Order by code
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByColumns(Builder $query, $direction)
    {
        $query->orderBy('columns', $direction);
    }
}
