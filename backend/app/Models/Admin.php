<?php

namespace App\Models;

use App\Notifications\NewPasswordNotification;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    CONST ACTIVE = 1;
    CONST INACTIVE = 0;

    public const DEFAULT_ORDER_SCOPE = 'defaultOrder';

    protected string $orderByColumn = 'id';

    protected string $orderByDirection = 'desc';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'name_kana',
        'email',
        'password',
        'type',
        'is_master',
        'is_active',
        'business_partner_id',
        'customer_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
        'password'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function booting()
    {
        static::addGlobalScope(
            self::DEFAULT_ORDER_SCOPE,
            function (Builder $builder) {
                $builder->scopes('defaultOrderBy');
            }
        );
    }

    /**
     * Set relation with table customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * Set relation with table business_partner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'business_partner_id', 'id');
    }

    /**
     * Scope a query to set a default order.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDefaultOrderBy(Builder $query): Builder
    {
        if (!$this->timestamps) {
            return $query;
        }

        return $query->orderBy($this->getTable() . '.' . $this->orderByColumn, $this->orderByDirection);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this));
    }

    /**
     * Send the new password notification.
     *
     * @param string $token
     * @return void
     */
    public function sendNewPasswordNotification($token)
    {
        $this->notify(new NewPasswordNotification($token, $this));
    }

    /**
     * Filter name
     *
     * @param Builder $query
     * @param varchar $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter by email
     *
     * @param Builder $query
     * @param $email
     * @return Builder
     */
    public function scopeEmail(Builder $query, $email): Builder
    {
        return $query->where('email', 'like', '%' . $email . '%');
    }

    /**
     * Filter by name_kana
     *
     * @param Builder $query
     * @param $nameKana
     * @return Builder
     */
    public function scopeNameKana(Builder $query, $nameKana): Builder
    {
        return $query->where('name_kana', 'like', '%' . $nameKana . '%');
    }

    /**
     * Filter by is_active
     *
     * @param Builder $query
     * @param $isActive
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $isActive): Builder
    {
        return $query->where('is_active', '=', $isActive);
    }

    /**
     * Filter by role
     *
     * @param Builder $query
     * @param $role
     * @return Builder
     */
    public function scopeIsMaster(Builder $query, $role): Builder
    {
        return $query->where('is_master', '=', $role);
    }

    /**
     * Filter by role
     *
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where('type', '=', $type);
    }

    /**
     * Hash password
     *
     * @param $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
