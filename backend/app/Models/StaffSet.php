<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class StaffSet extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [

        'set_id',
        'staff_id',
        'contract_id',
        'department_id',
        'quantity',
        'start_date',
        'end_date',
        'size',

    ];

    /**
     * Set relation with table sets
     *
     * @return BelongsTo
     */
    public function set()
    {
        return $this->belongsTo(Set::class, 'set_id', 'id');
    }

    /**
     * Set relation with table staffs
     * @return BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo(Staff::class,'staff_id', 'id');
    }
    /**
     * Set relation with table customer_departments
     * @return BelongsTo
     */
    public function contract()
    {
        return $this->belongsTo(Contract::class,'contract_id', 'id');
    }
     /**
     * Set relation with table customer_departments
     * @return BelongsTo
     */
    public function customerDepartment()
    {
        return $this->belongsTo(CustomerDepartment::class,'department_id', 'id');
    }

    // /**
    //  * Filter code
    //  *
    //  * @param Builder $query
    //  * @param $id
    //  *
    //  * @return Builder
    //  */
    // public function scopeId(Builder $query, $id): Builder
    // {
    //     return $query->where('customer_departments.id', 'like', '%' . $id . '%');
    // }

    // /**
    //  * Filter name
    //  *
    //  * @param Builder $query
    //  * @param $name
    //  *
    //  * @return Builder
    //  */
    // public function scopeName(Builder $query, $name): Builder
    // {
    //     return $query->where('customer_departments.name', 'like', '%' . $name . '%');
    // }

    // public function scopeStoreName(Builder $query, $name): Builder
    // {
    //     return $query->whereHas('customerStore', function (Builder $query) use ($name){
    //         $query->where('customer_stores.name', 'like', '%' . $name . '%');
    //     });
    // }

    // /**
    //  * Filter name of table customers
    //  *
    //  * @param Builder $query
    //  * @param $name
    //  *
    //  * @return Builder
    //  */
    // public function scopeCustomerName(Builder $query, $name): Builder
    // {
    //     return $query->whereHas('customer', function (Builder $query) use ($name){
    //         $query->where('customers.name', 'like', '%' . $name . '%');
    //     });
    // }

    // /**
    //  * Order by name of table customers
    //  * @param Builder $query
    //  * @param $direction
    //  */
    // public function scopeOrderByCustomerName(Builder $query, $direction)
    // {
    //     $query->withAggregate('customer:name', 'name')
    //         ->orderBy('customer_name', $direction);
    // }

    //  /**
    //  * Order by name of table customer_stores
    //  * @param Builder $query
    //  * @param $direction
    //  */
    // public function scopeOrderByStoreName(Builder $query, $direction)
    // {
    //     $query->withAggregate('customerStore:name', 'name')
    //         ->orderBy('customer_store_name', $direction);

    // }
}
