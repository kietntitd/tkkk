<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Builder;

class Item extends BaseModel
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'product_id',
        'size',
        'warning_quantity',
        'code',
        'name',
    ];

    /**
     * Set relation with product
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * Search by code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeCode(Builder $query, $code): Builder
    {
        return $query->where('code', 'like', '%' . $code . '%');
    }

    /**
     * Search by name
     *
     * @param Builder $query
     * @param $name
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Search Product by name
     * @param Builder $query
     * @param $productName
     */
    public function scopeProductName(Builder $query, $productName)
    {
        $query->whereHas('product', function ($query) use ($productName) {
            $query->where('name', 'like', '%' . $productName . '%');
        });
    }
    /**
     * Search Product by code
     * @param Builder $query
     * @param $productCode
     */
     public function scopeProductCode(Builder $query, $productCode)
     {
         $query->whereHas('product', function ($query) use ($productCode) {
             $query->where('code', 'like', '%' . $productCode . '%');
         });
     }


    /**
     * Search by size
     * @param Builder $query
     * @param $size
     * @return Builder
     */
    public function scopeSize(Builder $query, $size): Builder
    {
        return $query->where('size', 'like', '%' . $size . '%');
    }

    /**
     * Order by name of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductCode(Builder $query, $direction)
    {
        $query->withAggregate('product:code', 'code')
            ->orderBy('product_code', $direction);
    }

    /**
     * Order by name of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductName(Builder $query, $direction)
    {
        $query->withAggregate('product:name', 'name')
            ->orderBy('product_name', $direction);
    }


    /**
     * Search items by warning_quantity_from
     * @param Builder $query
     * @param $warning_quantity_from
     * @return Builder
     */
    public function scopeWarningQuantityFrom(Builder $query, $warning_quantity_from): Builder
    {
        return $query->where('items.warning_quantity', '>=', $warning_quantity_from);
    }

    /**
     * Search items by warning_quantity_to
     * @param Builder $query
     * @param $warning_quantity_to
     * @return Builder
     */
    public function scopeWarningQuantityTo(Builder $query, $warning_quantity_to): Builder
    {
        return $query->where('items.warning_quantity' ,'<=', $warning_quantity_to);
    }
}
