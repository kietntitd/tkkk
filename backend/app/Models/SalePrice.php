<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Builder;

class SalePrice extends BaseModel
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_sale_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'product_id',
        'customer_id',
        'rent_price',
        'sell_price',
        'wash_price',
        'start_date',
        'end_date',
        'parent_id',
    ];

    /**
     * Set relation with product
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * Set relation with customer
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }


    /**
     * Search Product by name
     * @param Builder $query
     * @param $productName
     */
    public function scopeProductName(Builder $query, $productName)
    {
        $query->whereHas('product', function (Builder $query) use ($productName) {
            $query->where('name', 'like', '%' . $productName . '%');
        });
    }

    /**
     * Search Product by code
     * @param Builder $query
     * @param $productCode
     */
       public function scopeProductCode(Builder $query, $productCode)
       {
           $query->whereHas('product', function (Builder $query) use ($productCode) {
               $query->where('code', 'like', '%' . $productCode . '%');
           });
       }

    /**
     * Search Product by id
     * @param Builder $query
     * @param $productId
     */
    public function scopeProductId(Builder $query, $productId)
    {
        $query->whereHas('product', function (Builder $query) use ($productId) {
            $query->where('id', '=', $productId);
        });
    }

    /**
     * Search Customer by name
     * @param  Builder $query
     * @param $customerName
     */

    public function scopeCustomerName(Builder $query, $customerName) {
        $query->whereHas('customer', function (Builder $query) use ($customerName){
            $query->where('name', 'like', '%' . $customerName . '%');
        });
    }

    /**
     * Search Customer by code
     * @param  Builder $query
     * @param $customerCode
     */
    public function scopeCustomerCode(Builder $query, $customerCode) {
        $query->whereHas('customer', function (Builder $query) use ($customerCode){
            $query->where('code', 'like', '%' . $customerCode . '%');
        });
    }

    /**
     * Order by name of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductName(Builder $query, $direction)
    {
        $query->withAggregate('product:name', 'name')
            ->orderBy('product_name', $direction);
    }

    /**
     * Order by code of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductCode(Builder $query, $direction)
    {
        $query->withAggregate('product:code', 'code')
            ->orderBy('product_code', $direction);
    }

    /**
     * Order by id of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductId(Builder $query, $direction)
    {
        $query->withAggregate('product:id', 'id')
            ->orderBy('product_id', $direction);
    }

    /**
     * Order by name of table customer
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByCustomerName(Builder $query, $direction)
    {
        $query->withAggregate('customer:name', 'name')
            ->orderBy('customer_name', $direction);
    }

    /**
     * Order by name of table customer
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByCustomerCode(Builder $query, $direction)
    {
        $query->withAggregate('customer:code', 'code')
            ->orderBy('customer_code', $direction);
    }
}

