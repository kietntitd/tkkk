<?php

namespace App\Models;

use App\Notifications\NewPasswordNotification;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    public const DEFAULT_ORDER_SCOPE = 'defaultOrder';

    protected string $orderByColumn = 'id';

    protected string $orderByDirection = 'desc';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'name_kana',
        'email',
        'is_active',
        'password',
        'customer_name_company',
        'partner_name_company',
        'business_partner_id',
        'customer_id',
        'type',
        'is_master',
    ];

    protected static function booting()
    {
        static::addGlobalScope(
            self::DEFAULT_ORDER_SCOPE,
            function (Builder $builder) {
                $builder->scopes('defaultOrderBy');
            }
        );
    }

    /**
     * Scope a query to set a default order.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDefaultOrderBy(Builder $query): Builder
    {
        if (!$this->timestamps) {
            return $query;
        }

        return $query->orderBy($this->getTable() . '.' . $this->orderByColumn, $this->orderByDirection);
    }

    /**
     * Type Warehouse of User
     */
    const TYPE_WAREHOUSE = '2';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this));
    }

    /**
     * Send the new password notification.
     *
     * @param string $token
     * @return void
     */
    public function sendNewPasswordNotification($token)
    {
        $this->notify(new NewPasswordNotification($token, $this));
    }

    /**
     * Set relation with table customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * Set relation with table business_partner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'business_partner_id', 'id');
    }

    /**
     * Filter user by name
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('users.name', 'like', '%' . $name . '%');
    }

    /**
     * Filter user by name_kana
     *
     * @param Builder $query
     * @param $nameKana
     *
     * @return Builder
     */
    public function scopeNameKana(Builder $query, $nameKana): Builder
    {
        return $query->where('users.name_kana', 'like', '%' . $nameKana . '%');
    }

    /**
     * Filter user by email
     *
     * @param Builder $query
     * @param $email
     * @return Builder
     */
    public function scopeEmail(Builder $query, $email): Builder
    {
        return $query->where('users.email', 'like', '%' . $email . '%');
    }

    /**
     * Filter user by is_active
     *
     * @param Builder $query
     * @param $isActive
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $isActive): Builder
    {
        return $query->where('users.is_active', '=', $isActive);
    }

    /**
     * Filter user by is_master
     * @param Builder $query
     * @param $isMaster
     * @return Builder
     */
    public function scopeIsMaster(Builder $query, $isMaster): Builder
    {
        return $query->where('users.is_master', '=', $isMaster);
    }

    /**
     * Filter user by name of table customers
     *
     * @param Builder $query
     * @param $customerNameCompany
     * @return Builder
     */
    public function scopeCustomerNameCompany(Builder $query, $customerNameCompany): Builder
    {
        return $query->where('customers.name', 'like', '%' . $customerNameCompany . '%');
    }

    /**
     * Filter user by name of table business_partners
     *
     * @param Builder $query
     * @param $partnerNameCompany
     * @return Builder
     */
    public function scopePartnerNameCompany(Builder $query, $partnerNameCompany): Builder
    {
        return $query->where('business_partners.name', 'like', '%' . $partnerNameCompany . '%');
    }

    /**
     * Filter user by company_name
     *
     * @param Builder $query
     * @param $customerNameCompany
     * @return Builder
     */
    public function scopeOrderByCustomerNameCompany(Builder $query, $customerNameCompany): Builder
    {
        return $query->orderBy('company_name', $customerNameCompany);
    }


    /**
     * Filter user by company_name
     *
     * @param Builder $query
     * @param $partnerNameCompany
     * @return Builder
     */
    public function scopeOrderByPartnerNameCompany(Builder $query, $partnerNameCompany): Builder
    {
        return $query->orderBy('company_name', $partnerNameCompany);
    }

    /**
     * Hash password
     *
     * @param $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Filter user by type
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where('users.type', '=', $type);
    }

    /**
     * Filter user by name of type customer in table users
     * @param Builder $query
     * @param $nameCustomer
     * @return Builder
     */
    public function scopeNameOfCustomerType(Builder $query, $nameCustomer): Builder
    {
        $query->whereHas('customer', function ($query) use ($nameCustomer) {
            $query->where('customers.name', 'like', '%' . $nameCustomer . '%');
        });
        return $query;
    }

    /**
     * Filter user by name of type business_partner in table users
     * @param Builder $query
     * @param $nameBusinessPartner
     * @return Builder
     */
    public function scopeNameOfBusinessPartnerType(Builder $query, $nameBusinessPartner): Builder
    {
        $query->whereHas('businessPartner', function ($query) use ($nameBusinessPartner) {
            $query->where('business_partners.name', 'like', '%' . $nameBusinessPartner . '%');
        });
        return $query;
    }

}
