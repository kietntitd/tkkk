<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends BaseModel
{

    const TYPE_SET = 'set';
    const TYPE_PRODUCT = 'product';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'name',
        'name_kana',
        'category_id',
        'factory_product_name',
        'factory_product_code',
        'is_active',
        'display_label',
        'price',
        'remarks',
        'label_type',
        'color',
        'business_partner_id',
        'customer_id',
        'type',
    ];


    /**
     * Set relation with table Category
     *
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * Set relation with table sets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function sets()
    {
        return $this->hasManyThrough(
            Set::class,
            SetProduct::class,
            'product_id',
            'id',
            'id',
            'set_id',
        );
    }

    /**
     * Set relation with table sale_prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function salePrices()
    {
        return $this->hasMany(SalePrice::class);
    }

    /**
     * Set relation with table business_partners
     *
     * @return belongsTo
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class);
    }

    public function setProduct()
    {
        return $this->hasMany(SetProduct::class, 'set_id', 'id');
    }

    /**
     * Set relation with table customer
     *
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * Filter by name
     *
     * @param Builder $query
     * @param $name
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter by code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeCode(Builder $query, $code): Builder
    {
        return $query->where('code', 'like', '%' . $code . '%');
    }

    /**
     * Filter by code
     *
     * @param Builder $query
     * @param param $id
     * @return Builder
     */
    public function scopeId(Builder $query, $id): Builder
    {
        return $query->where('id', 'like', '%' . $id . '%');
    }

    /**
     * Filter by name kana
     *
     * @param Builder $query
     * @param $nameKana
     * @return Builder
     */
    public function scopeNameKana(Builder $query, $nameKana): Builder
    {
        return $query->where('name_kana', 'like', '%' . $nameKana . '%');
    }

    /**
     * Filter by label name
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeDisplayLabel(Builder $query, $labelName): Builder
    {
        return $query->where('display_label', 'like', '%' . $labelName . '%');
    }

    /**
     * Filter by factory name
     *
     * @param Builder $query
     * @param $factoryName
     * @return Builder
     */
    public function scopeFactoryName(Builder $query, $factoryName): Builder
    {
        return $query->whereHas('businessPartner', function (Builder $query) use ($factoryName) {
            $query->where('name', 'like', '%' . $factoryName . '%');
        });
    }

    /**
     * Filter by factory code
     *
     * @param Builder $query
     * @param $factoryCode
     * @return Builder
     */
    public function scopeFactoryCode(Builder $query, $factoryCode): Builder
    {
        return $query->where('factory_code', 'like', '%' . $factoryCode . '%');
    }

    /**
     * Filter by factory production name
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeFactoryProductName(Builder $query, $factoryProducName): Builder
    {
        return $query->where('factory_product_name', 'like', '%' . $factoryProducName . '%');
    }

    /**
     * Filter by factory production code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeFactoryProductCode(Builder $query, $factoryProductCode): Builder
    {
        return $query->where('factory_product_code', 'like', '%' . $factoryProductCode . '%');
    }

    /**
     * Filter by code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeCategoryId(Builder $query, $categoryId): Builder
    {
        return $query->where('category_id', $categoryId);
    }

    /**
     * Filter by code
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $isActive): Builder
    {
        return $query->where('is_active', '=', $isActive);
    }

    /**
     * Filter by price min
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopePriceMin(Builder $query, $price): Builder
    {
        return $query->where('price', '>=', $price);
    }

    /**
     * Filter by price max
     *
     * @param Builder $query
     * @param $code
     * @return Builder
     */
    public function scopePriceMax(Builder $query, $price): Builder
    {
        return $query->where('price', '<=', $price);
    }

    /**
     * Filter category name
     *
     * @param Builder $query
     * @param $categoryName
     *
     * @return Builder
     */
    public function scopeCategoryName(Builder $query, $categoryName): Builder
    {
        return $query->whereHas('category', function (Builder $query) use ($categoryName) {
            $query->where('name', 'like', '%' . $categoryName . '%');
        });
    }

    /**
     * Filter by customer id
     *
     * @param Builder $query
     * @param $customerId
     * @return Builder
     */
    public function scopeCustomerId(Builder $query, $customerId): Builder
    {
        return $query->where('customer_id', $customerId);
    }

    /**
     * Filter by customer name
     *
     * @param Builder $query
     * @param $customerName
     * @return Builder
     */
    public function scopeCustomerName(Builder $query, $customerName): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($customerName){
            $query->where('name', 'like', '%' . $customerName . '%');
        });
    }

    /**
     * Filter by business partner id
     *
     * @param Builder $query
     * @param $businessPartnerId
     * @return Builder
     */
    public function scopeBusinessPartnerId(Builder $query, $businessPartnerId): Builder
    {
        return $query->where('business_partner_id', $businessPartnerId);
    }

    /**
     * Order by name of table category
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByCategoryName(Builder $query, $direction)
    {
        $query->withAggregate('category:name', 'name')
            ->orderBy('category_name', $direction);
    }

    /**
     * Order by name of table business_partners
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByFactoryName(Builder $query, $direction)
    {
        $query->withAggregate('businessPartner:name', 'name')
            ->orderBy('business_partner_name', $direction);
    }

    /**
     * Filter by type
     *
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where('type', $type);
    }

    /** Order by name of table category
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByStatus(Builder $query, $direction)
    {
        $query->orderBy('is_active', $direction);
    }

    /**
     * Order by company_name
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByCustomerName(Builder $query, $direction)
    {
        return $query->withAggregate('customer:name', 'name')
            ->orderBy('customer_name', $direction);
    }

    /**
     * Order by company_name
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByTotalProduct(Builder $query, $direction)
    {
        return $query->withCount('setProduct')
            ->orderBy('set_product_count', $direction);
    }
}
