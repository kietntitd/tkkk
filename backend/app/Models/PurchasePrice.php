<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Builder;

class PurchasePrice extends BaseModel
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_purchase_prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'business_partner_id',
        'product_id',
        'parent_id',
        'wash_price',
        'start_date',
        'end_date',
    ];

    /**
     * Set relation with product
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * Set relation with business partners
     * @return BelongsTo
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'business_partner_id', 'id');
    }

    /**
     * Search Product by name
     * @param Builder $query
     * @param $productName
     */
    public function scopeProductName(Builder $query, $productName)
    {
        $query->whereHas('product', function (Builder $query) use ($productName) {
            $query->where('name', 'like', '%' . $productName . '%');
        });
    }

    /**
     * Search Product by code
     * @param Builder $query
     * @param $productCode
     */
    public function scopeProductCode(Builder $query, $productCode)
    {
        $query->whereHas('product', function (Builder $query) use ($productCode) {
            $query->where('code', 'like', '%' . $productCode . '%');
        });
    }

    /**
     * Search Business Partner by name
     * @param  Builder $query
     * @param $businessPartnerName
     */

    public function scopeBusinessPartnerName(Builder $query, $businessPartnerName) {
        $query->whereHas('businessPartner', function (Builder $query) use ($businessPartnerName){
            $query->where('name', 'like', '%' . $businessPartnerName . '%');
        });
    }

    /**
     * Search Business Partner by code
     * @param  Builder $query
     * @param $businessPartnerCode
     */
    public function scopeBusinessPartnerCode(Builder $query, $businessPartnerCode) {
        $query->whereHas('businessPartner', function (Builder $query) use ($businessPartnerCode){
            $query->where('code', 'like', '%' . $businessPartnerCode . '%');
        });
    }

    /**
     * Order by name of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductName(Builder $query, $direction)
    {
        $query->withAggregate('product:name', 'name')
            ->orderBy('product_name', $direction);
    }

    /**
     * Order by code of table product
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByProductCode(Builder $query, $direction)
    {
        $query->withAggregate('product:code', 'code')
            ->orderBy('product_code', $direction);
    }

    /**
     * Order by name of table business partner
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByBusinessPartnerName(Builder $query, $direction)
    {
        $query->withAggregate('businessPartner:name', 'name')
            ->orderBy('business_partner_name', $direction);
    }

    /**
     * Order by code of table business partner
     * @param Builder $query
     * @param $direction
     */
    public function scopeOrderByBusinessPartnerCode(Builder $query, $direction)
    {
        $query->withAggregate('businessPartner:code', 'code')
            ->orderBy('business_partner_code', $direction);
    }
}
