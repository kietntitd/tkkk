<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class BusinessPartner extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $table = 'business_partners';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'code',
        'ctplan_code',
        'name',
        'name_kana',
        'postal_code',
        'address1',
        'address2',
        'tel',
        'fax',
        'email',
        'is_active',
    ];

    /**
     * BusinessPartner relation with table business_partners_rels
     *
     */
    public function businessPartnersRels()
    {
        return $this->hasMany(BusinessPartnersRel::class);
    }

    /**
     * self relation with table business_partners and business_partners_rels with business_partner_id = business_partners.id
     */
    public function children()
    {
        return $this->hasManyThrough(
            BusinessPartner::class,
            BusinessPartnersRel::class,
            'business_partner_id',
            'id',
            'id',
            'children_id',
        );
    }


    /**
     * Filter type
     *
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where('type', '=', $type);
    }

    /**
     * Filter id
     *
     * @param Builder $query
     * @param $id
     * @return Builder
     */
    public function scopeId(Builder $query, $id): Builder
    {
        return $query->where('id', 'like', '%' . $id . '%');
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $ctplanCode
     * @return Builder
     */
    public function scopeCtplanCode(Builder $query, $ctplanCode): Builder
    {
        return $query->where('ctplan_code', 'like', '%' . $ctplanCode . '%');
    }

    /**
     * Filter name
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter name_kana
     *
     * @param Builder $query
     * @param $nameKana
     * @return Builder
     */
    public function scopeNameKana(Builder $query, $nameKana): Builder
    {
        return $query->where('name_kana', 'like', '%' . $nameKana . '%');
    }

    /**
     * Filter address1
     *
     * @param Builder $query
     * @param $address1
     * @return Builder
     */
    public function scopeAddress1(Builder $query, $address1): Builder
    {
        return $query->where('address1', 'like', '%' . $address1 . '%');
    }

    /**
     * Filter tel
     *
     * @param Builder $query
     * @param $tel
     * @return Builder
     */
    public function scopeTel(Builder $query, $tel): Builder
    {
        return $query->where('tel', 'like', '%' . $tel . '%');
    }

    /**
     * Filter invoice_date
     *
     * @param Builder $query
     * @param $invoiceDate
     * @return Builder
     */
    public function scopeInvoiceDate(Builder $query, $invoiceDate): Builder
    {
        return $query->where('invoice_date', '=', $invoiceDate);
    }

    /**
     * Check type is cleaning
     * @param Builder $query
     * @param $type
     * @return Builder
     */

    public function scopeIsCleaning(Builder $query, $type): Builder
    {
        return $query->where('type', '=', $type);
    }
}
