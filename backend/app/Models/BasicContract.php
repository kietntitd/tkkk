<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class BasicContract extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'customer_id',
        'invoice_date',
        'payment_date',
        'remarks',
        'is_active',
    ];

    /**
     * Set relation with table customer
     *
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $id
     *
     * @return Builder
     */
    public function scopeId(Builder $query, $id): Builder
    {
        return $query->where('id', 'like', '%' . $id . '%');
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $code
     *
     * @return Builder
     */
    public function scopeCustomerCode(Builder $query, $code): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($code) {
            $query->where('code', 'like', '%' . $code . '%');
        });
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeCustomerName(Builder $query, $name): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($name) {
            $query->where('name', 'like', '%' . $name . '%');
        });
    }

    /**
     * Filter tel
     *
     * @param Builder $query
     * @param $tel
     *
     * @return Builder
     */
    public function scopeCustomerTel(Builder $query, $tel): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($tel) {
            $query->where('tel', 'like', '%' . $tel . '%');
        });
    }

    /**
     * Filter invoice_date
     *
     * @param Builder $query
     * @param $invoiceDate
     * @return Builder
     */
    public function scopeInvoiceDate(Builder $query, $invoiceDate): Builder
    {
        return $query->where('invoice_date', $invoiceDate);
    }

    /**
     * Filter pay_date
     *
     * @param Builder $query
     * @param $paymentDate
     * @return Builder
     */
    public function scopePaymentDate(Builder $query, $paymentDate): Builder
    {
        return $query->where('payment_date', $paymentDate);
    }

    /**
     * Sort by name of table customers
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByCustomerCode(Builder $query, $direction)
    {
        return $query->withAggregate('customer:code', 'code')
            ->orderBy('customer_code', $direction);
    }

    /**
     * Sort by name of table customers
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByCustomerName(Builder $query, $direction)
    {
        return $query->withAggregate('customer:name', 'name')
            ->orderBy('customer_name', $direction);
    }
}
