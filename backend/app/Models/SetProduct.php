<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\hasOne;

class SetProduct extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'set_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'set_id',
        'product_id',
        'quantity',
    ];

    /**
     * Set relation with table m_products
     *
     * @return hasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class);
    }

    /**
     * Filter name
     *
     * @param Builder $query
     * @param $setId
     * @return Builder
     */
    public function scopeSetId(Builder $query, $setId): Builder
    {
        return $query->where('set_id', 'like', '%' . $setId . '%');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
