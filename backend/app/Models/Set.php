<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Set extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'customer_id'
    ];
    

    /**
     * Set relation with table set_products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function setProducts()
    {
        return $this->hasMany(SetProduct::class);
    }

    /**
     * Set relation with table m_products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function products()
    {
        return $this->hasManyThrough(
            Product::class,
            SetProduct::class,
            'set_id',
            'id',
            'id',
            'product_id',
        );
    }

    /**
     * Set relation with table customers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Filter by name
     *
     * @param Builder $query
     * @param $name
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter by customer_id
     *
     * @param Builder $query
     * @param $customer_id
     * @return Builder
     */
    public function scopeCustomerName(Builder $query, $customerName): Builder
    {
        return $query->whereHas('customer', function ($query) use ($customerName) {
            $query->where('customers.name', 'like', '%' . $customerName . '%');
        });
    }

    /**
     * Order by company_name
     *
     * @param Builder $query
     * @param $customerNameCompany
     * @return Builder
     */
    public function scopeOrderByCustomerName(Builder $query, $direction)
    {
        return $query->withAggregate('customer:name', 'name')
            ->orderBy('customer_name', $direction);
    }

    /**
     * Order by company_name
     *
     * @param Builder $query
     * @param $customerNameCompany
     * @return Builder
     */
    public function scopeOrderByTotalProduct(Builder $query, $direction)
    {
        return $query->withCount('products')
            ->orderBy('products_count', $direction);
    }
}
