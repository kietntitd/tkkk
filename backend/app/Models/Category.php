<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Category extends BaseModel
{

    CONST ACTIVE = 1;
    CONST INACTIVE = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'note',
        'illustration_path',
        'illustration_file_name',
        'illustration_des',
        'is_active'
    ];

    /**
     * Filter category by name
     *
     * @param Builder $query
     * @param String $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter category by name
     *
     * @param Builder $query
     * @param int $value
     *
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $value): Builder
    {
        return $query->where('is_active', '=', $value);
    }
}
