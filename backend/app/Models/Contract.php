<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Contract extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'customer_id',
        'basic_contract_id',
        'store_id',
        'contract_type',
        'manage_type',
        'start_date',
        'end_date',
        'clothes_change',
        'pick_up_period',
        'business_partner_id',
        'tax',
        'wash_price',
        'remarks',
        'status',
    ];

    public function stores()
    {
        return $this->belongsTo(CustomerStore::class, 'store_id', 'id');
    }

    public function contractSet()
    {
        return $this->hasMany(ContractSet::class, 'contract_id', 'id');
    }

    public function basicContract()
    {
        return $this->belongsTo(BasicContract::class, 'basic_contract_id', 'id');
    }

    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'business_partner_id', 'id');
    }
}
