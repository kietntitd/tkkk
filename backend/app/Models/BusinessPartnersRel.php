<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class BusinessPartnersRel extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'business_partner_id',
        'children_id',
    ];

    /**
     * BusinessPartnersRel relation with table business_partners
     *
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class);
    }
}
