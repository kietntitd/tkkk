<?php

namespace App\Models;

use Helper\Common;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Customer extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'code',
        'ctplan_code',
        'name',
        'name_kana',
        'postal_code',
        'address1',
        'address2',
        'tel',
        'fax',
        'invoice_date',
        'note',
        'email',
        'is_active',
        'customer_id'
    ];

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $code
     *
     * @return Builder
     */
    public function scopeCode(Builder $query, $code): Builder
    {
        return $query->where('code', 'like', '%' . $code . '%');
    }

    /**
     * Filter name
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Filter name
     *
     * @param Builder $query
     * @param $address1
     *
     * @return Builder
     */
    public function scopeAddress1(Builder $query, $address1): Builder
    {
        return $query->where('address1', 'like', '%' . $address1 . '%');
    }

    /**
     * Filter by is_active
     *
     * @param Builder $query
     * @param $isActive
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $isActive): Builder
    {
        return $query->where('is_active', '=', $isActive);
    }

    /**
     * Hash password
     *
     * @param $value
     * @return void
     */
    public function setPickupCircleAttribute($value)
    {
        $this->attributes['pickup_circle'] = json_encode($value);
    }
}
