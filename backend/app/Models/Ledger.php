<?php

namespace App\Models;

use Helper\Common;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Ledger extends BaseModel
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'code',
        'customer_id',
        'tel',
        'fax',
        'address1',
        'address2',
        'pickup_circle',
        'job_type_id',
        'job_id',
        'area_id',
        'business_partner_id',
    ];

    /**
     * Filter name
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('customer_stores.name', 'like', '%' . $name . '%');
    }

    /**
     * Filter code
     *
     * @param Builder $query
     * @param $id
     *
     * @return Builder
     */
    public function scopeId(Builder $query, $id): Builder
    {
        return $query->where('customer_stores.id', 'like', '%' . $id . '%');
    }

    /**
     * Filter customer name
     *
     * @param Builder $query
     * @param $customerName
     *
     * @return Builder
     */
    public function scopeCustomerName(Builder $query, $customerName): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($customerName){
            $query->where('name', 'like', '%' . $customerName . '%');
        });
    }

    public function scopeCustomerId(Builder $query, $customerId): Builder
    {
        return $query->whereHas('customer', function (Builder $query) use ($customerId){
            $query->where('id', $customerId);
        });
    }

    /**
     * Set relation with table customer
     *
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
    public function contract()
    {
        return $this->belongsTo(Contract::class, 'customer_id', 'customer_id');
    }

    /**
     * Sort by name of table customers
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByCustomerName(Builder $query, $direction)
    {
        return $query->withAggregate('customer:name', 'name')
            ->orderBy('customer_name', $direction);
    }

    /**
     * Set relation with table store_attributes by area_id
     *
     * @return BelongsTo
     */
    public function storeAttributeWithAreaId()
    {
        //TODO use enum
        return $this->belongsTo(StoreAttribute::class, 'area_id', 'id')
            ->where('type', 'area');
    }

    /**
     * Set relation with table store_attributes by job_id
     *
     * @return BelongsTo
     */
    public function storeAttributeWithJobId()
    {
        //TODO use enum
        return $this->belongsTo(StoreAttribute::class, 'job_id', 'id')
            ->where('type', 'job_type');
    }

    /**
     * Set relation with table store_attributes by job_type_id
     *
     * @return BelongsTo
     */
    public function storeAttributeWithJobTypeId()
    {
        //TODO use enum
        return $this->belongsTo(StoreAttribute::class, 'job_type_id', 'id')
            ->where('type', 'business_type');
    }

    /**
     * Filter area_id
     *
     * @param Builder $query
     * @param $areaId
     * @return Builder
     */
    public function scopeAreaId(Builder $query, $areaId): Builder
    {
        return $query->whereHas('storeAttributeWithAreaId', function (Builder $query) use ($areaId){
            $query->where('id', $areaId);
        });
    }

    /**
     * Filter job_id
     *
     * @param Builder $query
     * @param $jobId
     * @return Builder
     */
    public function scopeJobId(Builder $query, $jobId): Builder
    {
        return $query->whereHas('storeAttributeWithJobId', function (Builder $query) use ($jobId){
            $query->where('id', $jobId);
        });
    }

    /**
     * Filter type_id
     *
     * @param Builder $query
     * @param $jobTypeId
     * @return Builder
     */
    public function scopeJobTypeId(Builder $query, $jobTypeId): Builder
    {
        return $query->whereHas('storeAttributeWithJobTypeId', function (Builder $query) use ($jobTypeId){
            $query->where('id', $jobTypeId);
        });
    }

    /**
     * Sort by area_id of table customer_stores
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByAreaId(Builder $query, $direction)
    {
        return $query->withAggregate('storeAttributeWithAreaId:name', 'name')
            ->orderBy('store_attribute_with_area_id_name', $direction);
    }


    /**
     * Sort by job_id of table customer_stores
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByJobId(Builder $query, $direction)
    {
        return $query->withAggregate('storeAttributeWithJobId:name', 'name')
            ->orderBy('store_attribute_with_job_id_name', $direction);
    }

    /**
     * Sort by job_type_id of table customer_stores
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByJobTypeId(Builder $query, $direction)
    {
        return $query->withAggregate('storeAttributeWithJobTypeId:name', 'name')
            ->orderBy('store_attribute_with_job_type_id_name', $direction);
    }

    /**
     * Set relation with table business_partner
     *
     * @return BelongsTo
     */
    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'business_partner_id', 'id');
    }

    /**
     * Filter customer name
     *
     * @param Builder $query
     * @param $businessPartnerId
     *
     * @return Builder
     */
    public function scopeBusinessPartnerId(Builder $query, $businessPartnerId): Builder
    {
        return $query->whereHas('businessPartner', function (Builder $query) use ($businessPartnerId){
            $query->where('name', 'like', '%' . $businessPartnerId . '%');
        });
    }

    /**
     * Sort by name of table business_partners
     *
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByBusinessPartnerId(Builder $query, $direction)
    {
        return $query->withAggregate('businessPartner:name', 'name')
            ->orderBy('business_partner_name', $direction);
    }
}
