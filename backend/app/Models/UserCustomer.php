<?php

namespace App\Models;

use App\Notifications\NewPasswordNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class UserCustomer extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;


    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'name_kana',
        'email',
        'is_active',
        'password',
        'is_master',
        'type',
    ];

    /**
     * Type Cleaning Factory of User
     */
    const TYPE_CLEANING_FACTORY = '3';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendNewPasswordNotification($token)
    {
        $this->notify(new NewPasswordNotification($token));
    }


    /**
     * Filter user by name
     *
     * @param Builder $query
     * @param $name
     *
     * @return Builder
     */
    public function scopeName(Builder $query, $name): Builder
    {
        return $query->where('users.name', 'like', '%' . $name . '%');
    }

    /**
     * Filter user by name_kana
     *
     * @param Builder $query
     * @param $nameKana
     *
     * @return Builder
     */
    public function scopeNameKana(Builder $query, $nameKana): Builder
    {
        return $query->where('users.name_kana', 'like', '%' . $nameKana . '%');
    }

    /**
     * Filter user by email
     *
     * @param Builder $query
     * @param $email
     * @return Builder
     */
    public function scopeEmail(Builder $query, $email): Builder
    {
        return $query->where('users.email', 'like', '%' . $email . '%');
    }

    /**
     * Filter user by is_active
     *
     * @param Builder $query
     * @param $isActive
     * @return Builder
     */
    public function scopeIsActive(Builder $query, $isActive): Builder
    {
        return $query->where('users.is_active', '=', $isActive);
    }

     /**
     * Filter user by is_master
     * @param Builder $query
     * @param $isMaster
     * @return Builder
     */
    public function scopeIsMaster(Builder $query, $isMaster): Builder
    {
        return $query->where('users.is_master', '=', $isMaster);
    }

    /**
     * Filter user by type
     *
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where('users.type', '=', $type);
    }


    /**
     * Hash password
     *
     * @param $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
