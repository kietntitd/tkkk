<?php

namespace App\Http\Controllers\Api;

use App\Http\Parameters\Criteria;
use App\Http\Requests\Meister\AuditRequest;
use App\Models\Role;
use App\Services\AuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuditController extends BaseController
{
    /**
     * @param AuditService $service
     * @param Request $request
     */
    public function __construct(AuditService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getRules(): string
    {
        return AuditRequest::class;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $isAdmin = auth()->user()->roles()->whereIn('name', Role::getRolePresetsName(Role::ROLE_ADMIN))->exists();
        $agentId = !empty(auth()->user()) && !$isAdmin ? auth()->user()->meister->agent_id : null;
        $list = $this->success(
            $this->service->list(
                Criteria::createFromRequest($this->request)
                    ->addFilter('agent_id', $agentId),
            )
        );
        return $list;
    }

    /**
     * @param Request $request
     * @param string $action
     * @return JsonResponse
     */
    public function logAction(Request $request, string $action): JsonResponse
    {
        $eventContent = $request->input('event_content', null);
        $log = $this->success(
            $this->service->writeLogAction($action, $eventContent)
        );
        return $log;
    }
}
