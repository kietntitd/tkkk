<?php

namespace App\Http\Controllers\Api\Clean;

use App\Http\Controllers\Api\BaseController;
use App\Http\Parameters\Criteria;
use App\Http\Requests\UserRequest;
use App\Services\UserCustomerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserCustomerController extends BaseController
{
    /**
     * UserController constructor.
     *
     * @param UserCustomerService $service
     *
     * @param Request $request
     */
    public function __construct(UserCustomerService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return UserRequest::class;
    }
    
}
