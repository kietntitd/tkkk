<?php

namespace App\Http\Controllers\Api\Clean;

use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\RequestPasswordRequest;
use App\Http\Requests\Admin\ResetPasswordRequest;
use App\Services\AuthenticationService;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @var AuthenticationService
     */
    private AuthenticationService $authenticationService;

    /**
     * @param AuthenticationService $authenticationService
     */
    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function login(LoginRequest $request)
    {
        $loginResponse = $this->authenticationService->login($request, 'api_user');
        if ($loginResponse->getCode() != Response::HTTP_OK) {
            return $this->error([], $loginResponse->getCode(), $loginResponse->getMessage());
        }
        return $this->success(
            [
                'access_token' => $loginResponse->getAccessToken(),
                'profile' => $loginResponse->getProfile(),
            ],
            $loginResponse->getCode(),
            $loginResponse->getMessage()
        );
    }

    public function requestPassword(RequestPasswordRequest $request)
    {
        $requestPasswordResponse = $this->authenticationService->requestPassword($request, 'users');
        if ($requestPasswordResponse->getCode() != Response::HTTP_OK) {
            return $this->error([], $requestPasswordResponse->getCode(), $requestPasswordResponse->getMessage());
        }
        return $this->success(
            [],
            $requestPasswordResponse->getCode(),
            $requestPasswordResponse->getMessage()
        );
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $resetPasswordResponse = $this->authenticationService->resetPassword($request, 'users');
        if ($resetPasswordResponse->getCode() != Response::HTTP_OK) {
            return $this->error([], $resetPasswordResponse->getCode(), $resetPasswordResponse->getMessage());
        }
        return $this->success(
            [],
            $resetPasswordResponse->getCode(),
            $resetPasswordResponse->getMessage()
        );
    }
}
