<?php

namespace App\Http\Controllers\Api\Clean;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\CleanRequest;
use App\Services\CleanService;
use Illuminate\Http\Request;

class CleanController extends BaseController
{
    /**
     * UserController constructor.
     *
     * @param  CleanService  $service
     *
     * @param  Request  $request
     */
    public function __construct(CleanService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return CleanRequest::class;
    }
}
