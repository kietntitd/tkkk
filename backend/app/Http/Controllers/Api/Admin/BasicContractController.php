<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\BasicContractRequest;
use App\Services\BasicContractService;
use Illuminate\Http\Request;

class BasicContractController extends BaseController
{

    /**
     * BasicContractController constructor.
     *
     * @param BasicContractService $service
     *
     * @param Request $request
     */
    public function __construct(
        BasicContractService $service,
        Request              $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return BasicContractRequest::class;
    }
}
