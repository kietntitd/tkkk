<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\PurchasePriceRequest;
use App\Services\BusinessPartnerService;
use App\Services\ProductService;
use App\Services\PurchasePriceService;
use App\Utils\MessageCommon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PurchasePriceController extends BaseController
{
    const MS03_057 = "Sản phẩm {0} đã được thiết định giá cho khách hàng này.";
    private BusinessPartnerService $businessPartnerService;
    private ProductService $productService;

    /**
     * PurchasePriceController constructor.
     *
     * @param PurchasePriceService $service
     *
     * @param Request $request
     * @param BusinessPartnerService $businessPartnerService
     * @param ProductService $productService
     */
    public function __construct(
        PurchasePriceService $service,
        Request $request,
        BusinessPartnerService $businessPartnerService,
        ProductService $productService
    ) {
        $this->businessPartnerService = $businessPartnerService;
        $this->productService = $productService;
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return PurchasePriceRequest::class;
    }

    /**
     * Create new model
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function store(): JsonResponse
    {
        try {
            $data = $this->validated();
            if (!$this->businessPartnerService->isExistBusinessPartner($data['business_partner_id'])) {
                return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS03_016_BUSINESS_PARTNER);
            }

            if ($data['products']) {
                $products = $data['products'];
                for ($i = 0; $i < count($products); $i++) {
                    if (!$this->productService->isExistsProduct($products[$i]['id'])) {
                        return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS03_043_PRODUCT);
                    }
                    if ($this->service->isExitsProductAndBusinessPartner($products[$i]['id'], $data['business_partner_id'])) {
                        return $this->error(
                            [],
                            Response::HTTP_UNPROCESSABLE_ENTITY,
                            str_replace("{0}", $products[$i]['name'], self::MS03_057)
                        );
                    }
                }
            }
            return $this->success(
                $this->service->create(
                    $this->validated()
                ),
                Response::HTTP_OK,
                MessageCommon::MS02_001
            );
        } catch (Exception $exception) {
            return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_004);
        }
    }

    /**
     * Get detail of model
     *
     * @param string $id
     *
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function show($id): JsonResponse
    {
        $keys = [
            'business_partner_id' => $this->request->business_partner_id,
            'product_id' => $this->request->product_id,
        ];
        $data = $this->service->findWithKeyBusiness($keys);
        if ($data) {
            return $this->success($data, Response::HTTP_OK);
        } else {
            return $this->error([], Response::HTTP_NOT_FOUND, MessageCommon::MS02_011);
        }
    }

    /**
     * Update model item data
     *
     * @param $id
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function update($id): JsonResponse
    {
        try {
            $data = $this->validated();
            if ($data['children']) {
                $children = $data['children'];
                foreach ($children as $child) {
                    if ($child['id'] && !$this->checkValidConcurrentTime($child)) {
                        return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_011);
                    }
                }
            }
            return $this->success(
                $this->service->update(
                    $id,
                    $this->validated()
                ),
                Response::HTTP_OK,
                MessageCommon::MS02_002
            );
        } catch (\Exception $exception) {
            return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_005);
        }
    }

    /**
     * Check if there are multi updates at the same time, same record
     *
     * @param $data
     * @return bool
     */
    protected function checkValidConcurrentTime($data)
    {
        $concurrentTime = strtotime($data['updated_at']);
        $updateAt = strtotime($this->service->find($data['id'])->updated_at);
        return $concurrentTime == $updateAt;
    }
}
