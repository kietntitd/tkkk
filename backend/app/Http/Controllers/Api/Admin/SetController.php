<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Parameters\Criteria;
use App\Http\Requests\SetRequest;
use App\Services\ProductService;
use App\Utils\MessageCommon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SetController extends BaseController
{
    /**
     * SetController constructor.
     *
     * @param ProductService $service
     *
     * @param Request $request
     */
    public function __construct(
        ProductService $service,
        Request        $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return SetRequest::class;
    }

    /**
     * @Override
     */
    public function index(): JsonResponse
    {
        return $this->success(
            $this->service->listSets(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    public function show($id): JsonResponse
    {
        return $this->success($this->service->findSet($id), Response::HTTP_OK);
    }

    /**
     * @Override
     */
    public function store(): JsonResponse
    {
        return $this->success(
            $this->service->createSet(
                $this->validated()
            ),
            Response::HTTP_OK,
            MessageCommon::MS02_001
        );
    }

    /**
     * @Override
     */
    public function update($id): JsonResponse
    {
        if (!$this->isValidConcurrentTime($id)) {
            return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_011);
        }
        return $this->success(
            $this->service->updateSet(
                $id,
                $this->validated()
            ),
            Response::HTTP_OK,
            MessageCommon::MS02_002
        );
    }

    /**
     * @Override
     */
    public function destroy($id): JsonResponse
    {
        if ($this->isValidConcurrentTime($id)) {
            $this->service->deleteSet($id);
            return $this->success(null, Response::HTTP_OK, MessageCommon::MS02_003);
        }
        return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_011);
    }
}
