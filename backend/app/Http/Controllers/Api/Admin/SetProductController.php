<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\SetProductRequest;
use App\Services\SetProductService;
use Illuminate\Http\Request;

class SetProductController extends BaseController
{
    /**
     * SetController constructor.
     *
     * @param SetProductService $service
     *
     * @param Request $request
     */
    public function __construct(SetProductService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return SetProductRequest::class;
    }
}
