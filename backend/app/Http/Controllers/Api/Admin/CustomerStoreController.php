<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\CustomerStoreRequest;
use App\Services\CustomerStoreService;
use Illuminate\Http\Request;

class CustomerStoreController extends BaseController
{

    /**
     * CustomerStoreController constructor.
     *
     * @param CustomerStoreService $service
     *
     * @param Request $request
     */
    public function __construct(
        CustomerStoreService $service,
        Request      $request
    )
    {
        parent::__construct($service, $request);
    }

    public function getRules(): string
    {
        return CustomerStoreRequest::class;
    }
}
