<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\LedgerRequest;
use Illuminate\Http\Response;
use App\Services\LedgerService;
//use App\Services\LedgerListService;
use Illuminate\Http\Request;
use App\Http\Parameters\Criteria;


class LedgerController extends BaseController
{
    /**
     * LedgerController constructor.
     *
     * @param  LedgerService  $service
     *
     * @param  Request  $request
     */
    public function __construct(
		LedgerService $service,
		//LedgerListService $service,
		Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return LedgerRequest::class;
    }

    public function getStaff($id)
    {
        $this->request->merge(['filter' => ['id' => $id]]);

        return $this->success(
            $this->service->listStaff(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );

    }
}
