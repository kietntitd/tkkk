<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Services\AdminService;
use App\Services\UserService;
use App\Utils\MessageCommon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends BaseController
{
    /**
     * UserController constructor.
     *
     * @param UserService $service
     * @param Request $request
     * @param AdminService $adminService
     */
    public function __construct(UserService $service, Request $request,protected AdminService $adminService)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return UserRequest::class;
    }

    /**
     * @Override
     */
    public function show($id): JsonResponse
    {
        $type = $this->request->get('type');
        return $this->success($this->service->findWithType($id, $type), Response::HTTP_OK);
    }

    /**
     * Check if there are multi updates at the same time, same record
     *
     */
    protected function isValidConcurrentTime($id)
    {
        $concurrentTime = strtotime($this->request->get('updated_at'));
        $type = $this->request->get('type');
        if ($type == User::TYPE_WAREHOUSE) {
            $updateAt = strtotime($this->adminService->find($id)->updated_at);
        } else {
            $updateAt = strtotime($this->service->find($id)->updated_at);
        }
        return $concurrentTime == $updateAt;
    }

    /**
     * Destroy model by hash id
     *
     * @param string $id
     *
     * @return JsonResponse
     *
     * @throws ModelNotFoundException
     * @throws Exception|\Exception
     */
    public function destroy($id): JsonResponse
    {
        if ($this->isValidConcurrentTime($id)) {
            $type = $this->request->get('type');
            if ($type == User::TYPE_WAREHOUSE) {
                $this->adminService->delete($id);
            } else {
                $this->service->delete($id);
            }
            return $this->success(null, Response::HTTP_OK, MessageCommon::MS02_003);
        }
        return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_011);
    }

}
