<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\ItemRequest;
use App\Services\ItemService;
use Illuminate\Http\Request;

class ItemController extends BaseController
{
    /**
     * ItemController constructor.
     *
     * @param  ItemService  $service
     *
     * @param  Request  $request
     */
    public function __construct(ItemService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return ItemRequest::class;
    }
}
