<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\ContractRequest;
use App\Services\ContractService;
use Illuminate\Http\Request;

class ContractController extends BaseController
{

    /**
     * { constructor.
     *
     * @param ContractService $service
     *
     * @param Request $request
     */
    public function __construct(
        ContractService $service,
        Request              $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return ContractRequest::class;
    }
}
