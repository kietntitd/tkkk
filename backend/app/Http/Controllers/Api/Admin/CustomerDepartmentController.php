<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\CustomerDepartmentRequest;
use App\Services\CustomerDepartmentService;
use Illuminate\Http\Request;

class CustomerDepartmentController extends BaseController
{

    /**
     * CustomerDepartmentController constructor.
     *
     * @param CustomerDepartmentService $service
     *
     * @param Request $request
     */
    public function __construct(
        CustomerDepartmentService $service,
        Request      $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return CustomerDepartmentRequest::class;
    }
}
