<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\BusinessPartnerRequest;
use App\Services\BusinessPartnerService;
use Illuminate\Http\Request;

class BusinessPartnerController extends BaseController
{

    /**
     * BusinessPartnerController constructor.
     *
     * @param BusinessPartnerService $service
     *
     * @param Request $request
     */
    public function __construct(
        BusinessPartnerService $service,
        Request                $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return BusinessPartnerRequest::class;
    }

}
