<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Parameters\Criteria;
use App\Services\BusinessPartnerService;
use App\Services\CategoryService;
use App\Services\CustomerService;
use App\Services\CustomerStoreService;
use App\Services\ProductService;
use App\Services\StoreAttributeService;
use App\Models\Category;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MasterController extends BaseController
{
    /**
     * @var Request
     * @var CustomerService
     * @var BusinessPartnerService
     * @var ProductService
     * @var StoreAttributeService
     */
    protected Request $request;
    protected CustomerService $customerService;
    protected BusinessPartnerService $businessPartnerService;
    protected ProductService $productService;
    protected CategoryService $categoryService;
    protected StoreAttributeService $storeAttributeService;
    protected CustomerStoreService $customerStoreService;

    /**
     * CustomerController constructor
     *
     * @param CustomerService $customerService
     * @param BusinessPartnerService $businessPartnerService
     * @param ProductService $productService
     * @param CategoryService $categoryService
     * @param StoreAttributeService $storeAttributeService
     * @param CustomerStoreService $customerStoreService
     * @param Request $request
     */
    public function __construct(
        CustomerService        $customerService,
        BusinessPartnerService $businessPartnerService,
        ProductService         $productService,
        CategoryService        $categoryService,
        StoreAttributeService  $storeAttributeService,
        CustomerStoreService   $customerStoreService,
        Request                $request
    )
    {
        $this->request = $request;
        $this->customerService = $customerService;
        $this->businessPartnerService = $businessPartnerService;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->storeAttributeService = $storeAttributeService;
        $this->customerStoreService = $customerStoreService;
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return '';
    }

    /**
     * Get list model items for the dropdown
     *
     * @return JsonResponse
     */
    public function getCustomerOptions(): JsonResponse
    {
        $this->request->merge(['filter' => ['deleted_at' => null]]);
        return $this->success(
            $this->customerService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    /**
     * Get list model items for the dropdown
     *
     * @return JsonResponse
     */
    public function getBusinessPartnerOptions(): JsonResponse
    {
        $this->request->merge(['filter' => ['deleted_at' => null]]);
        return $this->success(
            $this->businessPartnerService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    public function getCategoryOptions(): JsonResponse
    {
        $this->request->merge(['filter' => [
            'deleted_at' => null,
            'is_active' => Category::ACTIVE,
        ]]);
        return $this->success(
            $this->categoryService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    /**
     * Get list model items for the dropdown
     *
     * @return JsonResponse
     */
    public function getProductOptions(): JsonResponse
    {
        $this->request->merge(['filter' => ['deleted_at' => null]]);
        return $this->success(
            $this->productService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    /**
     * Get list model items for the dropdown table store_attributes
     *
     * @return JsonResponse
     */
    public function getStoreAttributeOptions(): JsonResponse
    {
        return $this->success(
            $this->storeAttributeService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }

    /**
     * Get list model items for the dropdown table store_attributes
     *
     * @return JsonResponse
     */
    public function getCustomerStoreOptions(): JsonResponse
    {
        return $this->success(
            $this->customerStoreService->getDataOptions(Criteria::createFromRequest($this->request)),
            Response::HTTP_OK
        );
    }
}
