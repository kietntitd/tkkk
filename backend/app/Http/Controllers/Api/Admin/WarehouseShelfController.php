<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\WarehouseShelfRequest;

use App\Services\WarehouseShelfService;

use Illuminate\Http\Request;

class WarehouseShelfController extends BaseController
{
    /**
     * WarehouseShelfController constructor.
     *
     * @param  WarehouseShelfService  $service
     *
     * @param  Request  $request
     */
    public function __construct(WarehouseShelfService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return WarehouseShelfRequest::class;
    }
}
