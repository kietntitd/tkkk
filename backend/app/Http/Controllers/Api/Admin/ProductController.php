<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Parameters\Criteria;

use App\Services\ProductService;
use App\Http\Requests\ProductRequest;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends BaseController
{
    /**
     * UserController constructor.
     *
     * @param  ProductService  $service
     *
     * @param  Request  $request
     */
    public function __construct(ProductService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return ProductRequest::class;
    }
}
