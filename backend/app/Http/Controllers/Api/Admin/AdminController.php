<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\AdminRequest;
use App\Services\AdminService;
use Illuminate\Http\Request;

class AdminController extends BaseController
{

    /**
     * UserController constructor.
     *
     * @param AdminService $service
     *
     * @param Request $request
     */
    public function __construct(
        AdminService $service,
        Request      $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return AdminRequest::class;
    }
}
