<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\SalePriceRequest;
use App\Services\CustomerService;
use App\Services\ProductService;
use App\Services\SalePriceService;
use App\Utils\MessageCommon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SalePriceController extends BaseController
{
    private CustomerService $customerService;
    private ProductService $productService;

    /**
     * SalePriceController constructor.
     *
     * @param SalePriceService $service
     *
     * @param Request $request
     * @param CustomerService $customerService
     * @param ProductService $productService
     */
    public function __construct(
        SalePriceService $service,
        Request $request,
        CustomerService $customerService,
        ProductService $productService
    ) {
        $this->customerService = $customerService;
        $this->productService = $productService;
        parent::__construct($service, $request);
    }

    /**
     * Get FormRequest validation
     *
     * @return string
     */
    public function getRules(): string
    {
        return SalePriceRequest::class;
    }

    /**
     * Get detail of model
     *
     * @param string $id
     *
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function show($id): JsonResponse
    {
        $keys = [
            'customer_id' => $this->request->customer_id,
            'product_id' => $this->request->product_id,
        ];
        $data = $this->service->findWithKeyBusiness($keys);
        if ($data) {
            return $this->success($data, Response::HTTP_OK);
        } else {
            return $this->error([], Response::HTTP_NOT_FOUND, MessageCommon::MS02_011);
        }
    }

    /**
     * Create new model
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function store(): JsonResponse
    {
        try {
            $data = $this->validated();
            if (!$this->customerService->isExistCustomer($data['customer_id'])) {
                return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS03_043_CUSTOMER);
            }

            if ($data['products']) {
                $products = $data['products'];
                for ($i = 0; $i < count($products); $i++) {
                    if (!$this->productService->isExistsProduct($products[$i]['id'])) {
                        return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS03_043_PRODUCT);
                    }
                    if ($this->service->isExitsProductAndCustomer($products[$i]['id'], $data['customer_id'])) {
                        return $this->error(
                            [],
                            Response::HTTP_UNPROCESSABLE_ENTITY,
                            __('exception.messages.MS03_057', ['product' => $products[$i]['name'], 'customer' => $data['customer']['name']])
                        );
                    }
                }
            }
            return $this->success(
                $this->service->create(
                    $this->validated()
                ),
                Response::HTTP_OK,
                MessageCommon::MS02_001
            );
        } catch (\Exception $exception) {
            return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_004);
        }
    }

    /**
     * Update model item data
     *
     * @param $id
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function update($id): JsonResponse
    {
        try {
            $data = $this->validated();
            if ($data['children']) {
                $children = $data['children'];
                foreach ($children as $child) {
                    if ($child['id'] && !$this->checkValidConcurrentTime($child)) {
                        return $this->error(null, Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_011);
                    }
                }
            }
            return $this->success(
                $this->service->update(
                    $id,
                    $this->validated()
                ),
                Response::HTTP_OK,
                MessageCommon::MS02_002
            );
        } catch (\Exception $exception) {
            return $this->error([], Response::HTTP_UNPROCESSABLE_ENTITY, MessageCommon::MS02_005);
        }
    }

    /**
     * Check if there are multi updates at the same time, same record
     *
     * @param $data
     * @return bool
     */
    protected function checkValidConcurrentTime($data)
    {
        $concurrentTime = strtotime($data['updated_at']);
        $updateAt = strtotime($this->service->find($data['id'])->updated_at);
        return $concurrentTime == $updateAt;
    }
}
