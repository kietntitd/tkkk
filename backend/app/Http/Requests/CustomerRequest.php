<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class CustomerRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'code' => [
                'required',
            ],
            'ctplan_code' => 'nullable|min:8|max:8',
            'name' => [
                'required',
                'max:255',
                Rule::unique('customers')
                    ->where('name', $this->request->get('name'))
                    ->whereNull('deleted_at'),
            ],
            'name_kana' => V_NAME_KANA,
            'postal_code' => 'required|min:7|max:8',
            'address1' => 'required|max:255',
            'address2' => 'nullable|max:255',
            'tel' => 'required|max:20',
            'fax' => 'max:20',
            'invoice_date' => 'required',
            'note' => 'nullable|max:1000',
            'email' => [
                'nullable',
                'email',
                'max:255',
            ],
            'is_active' => 'required',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'code' => [
                'nullable',
            ],
            'ctplan_code' => 'nullable|min:8|max:8',
            'name' => [
                'required',
                'max:255',
                Rule::unique('customers')
                    ->whereNot('id', $this->request->get('id'))
                    ->where('name', $this->request->get('name'))
                    ->whereNull('deleted_at'),
            ],
            'name_kana' => V_NAME_KANA,
            'postal_code' => 'required|min:7|max:8',
            'address1' => 'required|max:255',
            'address2' => 'nullable|max:255',
            'tel' => 'required|max:20',
            'fax' => 'max:20',
            'invoice_date' => 'required',
            'note' => 'max:1000',
            'email' => [
                'nullable',
                'email',
                'max:255',
            ],
            'is_active' => 'required',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.customers.code'),
            'ctplan_code' => __('field.customers.ctplan_code'),
            'name' => __('field.customers.name'),
            'name_kana' => __('field.customers.name_kana'),
            'postal_code' => __('field.customers.postal_code'),
            'address1' => __('field.customers.address1'),
            'address2' => __('field.customers.address2'),
            'tel' => __('field.customers.tel'),
            'fax' => __('field.customers.fax'),
            'invoice_date' => __('field.customers.invoice_date'),
            'note' => __('field.customers.note'),
            'email' => __('field.customers.email'),
        ];
    }
}
