<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class AdminRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        $id = $this->id;
        return [
            'name' => V_VARCHAR_REQUIRED,
            'name_kana' => V_NAME_KANA,
            'email' => [
                'required',
                'email',
                'max:255',
                'unique:admins,email,'.$id.',id,deleted_at,NULL',
                'unique:users,email,NULL,id,deleted_at,NULL',
            ],
            'is_master' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'name' => V_VARCHAR_REQUIRED,
            'name_kana' => V_NAME_KANA,
            'email' => [
                'required',
                'email',
                'max:255',
            ],
            'is_master' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'role' => __('field.admins.code'),
            'name' => __('field.admins.name'),
            'name_kana' => __('field.admins.name_kana'),
            'email' => __('field.admins.email'),
            'status' => __('field.admins.status'),
        ];
    }
}
