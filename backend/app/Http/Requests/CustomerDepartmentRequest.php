<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;


class CustomerDepartmentRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'code' => [
                'nullable',
                'max:64',
                Rule::unique('customer_departments')
                ->where('code', $this->request->get('code'))
                ->where('store_id', $this->request->get('store_id'))
                ->whereNull('deleted_at')
            ],
            'name' => [
                'required',
                'max:255',
                Rule::unique('customer_departments')
                    ->where('name', $this->request->get('name'))
                    ->where('store_id', $this->request->get('store_id'))
                    ->whereNull('deleted_at')
            ],
            'customer_id' => [
                'required',
                Rule::exists('customers', 'id')
                    ->whereNull('deleted_at')
            ],
            'store_id' => [
                'required',
                Rule::exists('customer_stores', 'id')
                    ->whereNull('deleted_at')
            ]
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'code' => [
                'nullable',
                'max:64',
                Rule::unique('customer_departments')
                    ->whereNot('id', $this->request->get('id'))
                    ->where('code', $this->request->get('code'))
                    ->where('store_id', $this->request->get('store_id'))
                    ->whereNull('deleted_at'),
            ],
            'name' => [
                'required',
                'max:255',
                Rule::unique('customer_departments')
                    ->whereNot('id', $this->request->get('id'))
                    ->where('name', $this->request->get('name'))
                    ->where('store_id', $this->request->get('store_id'))
                    ->whereNull('deleted_at')
            ],
            'customer_id' => [
                'required',
                Rule::exists('customers', 'id')
                    ->whereNull('deleted_at')
            ],
            'store_id' => [
                'required',
                Rule::exists('customer_stores', 'id')
                    ->whereNull('deleted_at')
            ]
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.customer_departments.department_code'),
            'name' => __('field.customer_departments.department_name'),
            'customer_id' => __('field.customer_departments.customer_name'),
            'store_id' => __('field.customer_departments.store_name')
        ];
    }
}
