<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class CustomerStoreRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'code' => [
                'required',
                'max:64',
                Rule::unique('customer_stores')
                    ->where('code', $this->request->get('code'))
                    ->whereNull('deleted_at'),
            ],
            'name' => 'required|max:255',
            'tel' => 'required|max:20',
            'fax' => 'required|max:20',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'business_partner_id' => 'required|exists:business_partners,id',
            'area_id' => 'required',
            'job_id' => 'required',
            'job_type_id' => 'required',
            'customer_id' => 'required|exists:customers,id',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'code' => [
                'required',
                'max:64',
                Rule::unique('customer_stores')
                    ->whereNot('id', $this->request->get('id'))
                    ->where('code', $this->request->get('code'))
                    ->whereNull('deleted_at'),
            ],
            'name' => 'required|max:255',
            'tel' => 'required|max:20',
            'fax' => 'required|max:20',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'business_partner_id' => 'required|exists:business_partners,id',
            'area_id' => 'required',
            'job_id' => 'required',
            'job_type_id' => 'required',
            'customer_id' => 'required|exists:customers,id',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.customer_stores.store_code'),
            'name' => __('field.customer_stores.store_name'),
            'tel' => __('field.customer_stores.tel'),
            'fax' => __('field.customer_stores.fax'),
            'address1' => __('field.customer_stores.address1'),
            'address2' => __('field.customer_stores.address2'),
            'customer_id' => __('field.customer_stores.customer_name'),
            'area_id' => __('field.customer_stores.area_id'),
            'job_id' => __('field.customer_stores.job_id'),
            'job_type_id' => __('field.customer_stores.job_type_id'),
            'business_partner_id' => __('field.customer_stores.business_partner_id'),
        ];
    }
}
