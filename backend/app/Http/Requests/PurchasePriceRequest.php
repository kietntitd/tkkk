<?php

namespace App\Http\Requests;

class PurchasePriceRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [];
    }
}
