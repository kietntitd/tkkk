<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ItemRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'code' => [
                'required',
                Rule::unique('items')
                    ->where('code', $this->request->get('code'))
                    ->whereNull('deleted_at'),
            ],
            'name' => [
                'required',
                'max:255',
                Rule::unique('items')
                    ->where('name', $this->request->get('name'))
                    ->whereNull('deleted_at'),
            ],

            'product_id' => [
                'required',
                Rule::exists('m_products', 'id')
                    ->whereNull('deleted_at'),
            ],
            'size' => [
                'required',
                'max:5',
                Rule::unique('items')
                    ->where('product_id', $this->request->get('product_id'))
                    ->where('size', $this->request->get('size'))
                    ->whereNull('deleted_at'),
            ],
            'warning_quantity' => [
                'integer',
                'min:1',
                'max:999999999',
            ],
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'code' => [
                'required',
                Rule::unique('items')
                    ->where('code', $this->request->get('code'))
                    ->whereNot('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
            ],
            'name' => [
                'required',
                'max:255',
                Rule::unique('items')
                    ->where('name', $this->request->get('name'))
                    ->whereNot('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
            ],
            'product_id' => [
                'required',
                Rule::exists('m_products', 'id')
                    ->whereNull('deleted_at'),
            ],
            'size' => [
                'required',
                'max:5',
                Rule::unique('items')
                    ->where('product_id', $this->request->get('product_id'))
                    ->where('size', $this->request->get('size'))
                    ->whereNot('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
            ],
            'warning_quantity' => [
                'integer',
                'min:1',
                'max:999999999',
            ],
        ];
    }

    /**
     * @Override
     */
    public function messages()
    {
        return [
            'size.unique' => __('validation.unique_items', [
                'attribute' => __('field.items.name'),
                'values' => $this->request->get('size'),
            ]),
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.items.code'),
            'name' => __('field.items.name'),
            'product_id' => __('field.items.product_id'),
            'size' => __('field.items.size'),
            'warning_quantity' => __('field.items.quantity'),
        ];
    }
}

