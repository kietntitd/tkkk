<?php

namespace App\Http\Requests;

class CategoryRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        $id = $this->id;
        return [
            'name' => 'required|unique:m_categories,name,'.$id.',id,deleted_at,NULL',
            'illustration_image' => 'nullable|image|max:5120|mimes:jpeg,png|dimensions:max_width=1920,max_height=1080',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'name' => 'required',
            'illustration_image' => 'nullable|image|max:5120|mimes:jpeg,png|dimensions:max_width=1920,max_height=1080',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('field.categories.name'),
        ];
    }
}
