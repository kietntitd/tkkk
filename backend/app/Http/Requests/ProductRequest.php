<?php

namespace App\Http\Requests;


use Illuminate\Validation\Rule;

class ProductRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'name' => 'required',
            'category_id' => [
                'required',
                Rule::exists('m_categories', 'id')->whereNull('deleted_at'),
            ],
            'price' => 'required',
            'label_type' => 'required',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'name' => 'required',
            'category_id' => [
                'required',
                Rule::exists('m_categories', 'id')->whereNull('deleted_at'),
            ],
            'price' => 'required',
            'label_type' => 'required',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('field.products.name'),
            'category_id' => __('field.products.category_id'),
            'price' => __('field.products.price'),
            'business_partner_id' => __('field.products.business_partner_id'),
            'label_type' => __('field.products.label_type'),
        ];
    }
}
