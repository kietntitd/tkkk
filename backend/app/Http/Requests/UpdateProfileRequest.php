<?php

namespace App\Http\Requests;

use App\Rules\FaxRule;
use App\Rules\KatakanaRule;
use App\Rules\MobileRule;
use App\Rules\PostCodeRule;
use App\Rules\TelRule;
use Enum\UserTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'mobile' => ['nullable', new MobileRule()],
            'tel' => ['nullable', new TelRule()],
            'fax' => ['nullable', new FaxRule()],
            'kana_name' => ['nullable', new KatakanaRule()],
            'birthdate' => 'nullable|date_format:Y-m-d',
            'post_code' => ['nullable', new PostCodeRule()],
            'type' => ['nullable', Rule::in(collect(UserTypeEnum::getLists())->pluck('id')->all())],
        ];
    }
}
