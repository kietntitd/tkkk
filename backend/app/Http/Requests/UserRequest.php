<?php

namespace App\Http\Requests;

class UserRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        $id = $this->id;
        return [
            'name' => V_VARCHAR_REQUIRED,
            'name_kana' => V_NAME_KANA,
            'email' => [
                'required',
                'email',
                'max:255',
                'unique:users,email,'.$id.',id,deleted_at,NULL',
                'unique:admins,email,NULL,id,deleted_at,NULL',
            ],
            'is_master' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'name' => V_VARCHAR_REQUIRED,
            'name_kana' => V_NAME_KANA,
            'email' => [
                'required',
                'email',
                'max:255',
            ],
            'is_master' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('field.users.name'),
            'name_kana' => __('field.users.name_kana'),
            'email' => __('field.users.email'),
            'status' => __('field.users.status'),
        ];
    }
}
