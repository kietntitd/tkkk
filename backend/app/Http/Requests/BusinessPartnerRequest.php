<?php

namespace App\Http\Requests;

use App\Utils\MessageCommon;
use Illuminate\Validation\Rule;

class BusinessPartnerRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'type' => 'required',
            'code' => 'nullable|max:64',
            'name' => [
                'required',
                'max:255',
                Rule::unique('business_partners')
                    ->where('name', $this->request->get('name'))
                    ->where('type', $this->request->get('type'))
                    ->whereNull('deleted_at'),
            ],
            'name_kana' => V_NAME_KANA,
            'postal_code' => 'required|min:7|max:8',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'tel' => 'required|max:14',
            'fax' => 'nullable|max:14',
            'email' => 'nullable|email|max:255',
            'is_active' => 'required',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'type' => 'required',
            'code' => 'nullable|max:64',
            'name' => [
                'required',
                'max:255',
                Rule::unique('business_partners')
                    ->where('name', $this->request->get('name'))
                    ->whereNot('id', $this->request->get('id'))
                    ->where('type', $this->request->get('type'))
                    ->whereNull('deleted_at'),
            ],
            'name_kana' => V_NAME_KANA,
            'postal_code' => 'required|min:7|max:8',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'tel' => 'required|max:14',
            'fax' => 'nullable|max:14',
            'email' => 'nullable|email|max:255',
            'is_active' => 'required',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.business_partners.code'),
            'ctplan_code' => __('field.business_partners.ctplan_code'),
            'name' => __('field.business_partners.name'),
            'name_kana' => __('field.business_partners.name_kana'),
            'postal_code' => __('field.business_partners.postal_code'),
            'address1' => __('field.business_partners.address1'),
            'address2' => __('field.business_partners.address2'),
            'tel' => __('field.business_partners.tel'),
            'fax' => __('field.business_partners.fax'),
            'note' => __('field.business_partners.note'),
            'email' => __('field.business_partners.email'),
        ];
    }
}
