<?php

namespace App\Http\Requests;

class WarehouseShelfRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'code' => 'required|unique:m_shelves,code,NULL,id,deleted_at,NULL',
            'rows' => 'required|numeric',
            'columns' => 'required|numeric',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        $id = $this->id;
        return [
            'code' => 'required|unique:m_shelves,code,' . $id . ',id,deleted_at,NULL',
            'rows' => 'required|numeric',
            'columns' => 'required|numeric',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => __('field.shelves.code'),
            'rows' => __('field.shelves.rows'),
            'columns' => __('field.shelves.columns'),
        ];
    }
}
