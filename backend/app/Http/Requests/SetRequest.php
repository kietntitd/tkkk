<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class SetRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'name' => [
                'required',
                Rule::unique('m_products')
                    ->where('name', $this->request->get('name'))
                    ->where('type', $this->request->get('type'))
                    ->whereNull('deleted_at'),
            ],
            'customer_id' => [
                'required',
                Rule::exists('customers', 'id')
                    ->where('id', $this->request->get('customer_id'))
                    ->where('is_active', 1)
                    ->whereNull('deleted_at')
            ],
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'name' => [
                'required',
                Rule::unique('m_products')
                    ->where('name', $this->request->get('name'))
                    ->where('type', $this->request->get('type'))
                    ->whereNot('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
            ],
            'customer_id' => [
                'required',
                Rule::exists('customers', 'id')
                    ->where('id', $this->request->get('customer_id'))
                    ->where('is_active', 1)
                    ->whereNull('deleted_at')
            ],
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('field.sets.name'),
            'customer_id' => __('field.sets.customer_id'),
        ];
    }
}
