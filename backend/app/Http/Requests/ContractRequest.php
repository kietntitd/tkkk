<?php

namespace App\Http\Requests;

use App\Utils\MessageCommon;
use Illuminate\Validation\Rule;

class ContractRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'basic_contract_id' => 'required',
            'store_id' => [
                'required',
                Rule::exists('customer_stores', 'id')
                    ->whereNull('deleted_at'),
                Rule::unique('contracts')
                    ->where('store_id', $this->request->get('store_id'))
                    ->whereNull('deleted_at'),
            ],
            'start_date' => [
                'nullable',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'nullable',
                'after_or_equal:start_date',
            ],
            'contract_type' => 'required',
            'manage_type' => '',
            'business_partner_id' => '',
            'tax' => 'required',
            'wash_price' => '',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'basic_contract_id' => 'required',
            'store_id' => [
                'required',
                Rule::exists('customer_stores', 'id')
                    ->whereNull('deleted_at'),
                Rule::unique('contracts')
                    ->where('store_id', $this->request->get('store_id'))
                    ->whereNull('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
            ],
            'start_date' => [
                'nullable',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'nullable',
                'after_or_equal:start_date',
            ],
            'contract_type' => 'required',
            'manage_type' => '',
            'business_partner_id' => '',
            'tax' => 'required',
            'wash_price' => '',
        ];
    }

    /**
     * @Override
     */
    public function messages()
    {
        return [
            'store_id.unique' => __('validation.unique_contracts'),
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'basic_contract_id' => __('field.contracts.contracts_detail.basic_contract_id'),
            'store_id' => __('field.contracts.contracts_detail.store_id'),
            'contract_type' => __('field.contracts.contracts_detail.contract_type'),
            'manage_type' => __('field.contracts.contracts_detail.manage_type'),
            'start_date' => __('field.contracts.contracts_detail.start_date'),
            'end_date' => __('field.contracts.contracts_detail.end_date'),
            'clothes_change' => __('field.contracts.contracts_detail.clothes_change'),
            'pick_up_period' => __('field.contracts.contracts_detail.pick_up_period'),
            'business_partner_id' => __('field.contracts.contracts_detail.business_partner_id'),
            'tax' => __('field.contracts.contracts_detail.tax'),
            'wash_price' => __('field.contracts.contracts_detail.wash_price'),
            'remarks' => __('field.contracts.contracts_detail.remarks'),
        ];
    }
}
