<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class BasicContractRequest extends BaseRequest
{
    /**
     * Define rules for store function
     *
     * @return array
     */
    public function rulesPost()
    {
        return [
            'customer_id' => [
                'required',
                'max:64',
                Rule::unique('basic_contracts')
                    ->where('customer_id', $this->request->get('customer_id'))
                    ->whereNull('deleted_at'),
                Rule::exists('customers', 'id')
                    ->where('is_active', 1)
                    ->whereNull('deleted_at'),
            ],
            'invoice_date',
            'payment_date',
            'remarks',
        ];
    }

    /**
     * Define rules for update function
     *
     * @return array
     */
    public function rulesPut()
    {
        return [
            'customer_id' => [
                'required',
                'max:64',
                Rule::unique('basic_contracts')
                    ->where('customer_id', $this->request->get('customer_id'))
                    ->whereNot('id', $this->request->get('id'))
                    ->whereNull('deleted_at'),
                Rule::exists('customers', 'id')
                    ->where('is_active', 1)
                    ->whereNull('deleted_at'),
            ],
            'invoice_date',
            'payment_date',
            'remarks',
        ];
    }

    /**
     * Bind attributes into message
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'customer_id' => __('field.contracts.customer_id'),
            'invoice_date' => __('field.contracts.invoice_date'),
            'payment_date' => __('field.contracts.payment_date'),
            'remarks' => __('field.contracts.remarks'),
        ];
    }
}
