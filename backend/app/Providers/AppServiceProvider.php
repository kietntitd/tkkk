<?php

namespace App\Providers;

use App\Macros\StrMixins;
use App\Repositories\AdminRepositoryInterface;
use App\Repositories\BarCodeRepositoryInterface;
use App\Repositories\BaseRepositoryInterface;
use App\Repositories\BusinessPartnerRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\BasicContractRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Repositories\ContractSetRepositoryInterface;
use App\Repositories\CustomerDepartmentRepositoryInterface;
use App\Repositories\CustomerRepositoryInterface;
use App\Repositories\Impl\BarCodeRepository;
use App\Repositories\Impl\BasicContractRepository;
use App\Repositories\Impl\ContractRepository;
use App\Repositories\Impl\ContractSetRepository;
use App\Repositories\Impl\ItemRepository;
use App\Repositories\Impl\SetProductRepository;
use App\Repositories\Impl\StoreAttributeRepository;
use App\Repositories\ItemRepositoryInterface;
use App\Repositories\SetProductRepositoryInterface;
use App\Repositories\StoreAttributeRepositoryInterface;
use App\Repositories\Impl\UserCustomerRepository;
use App\Repositories\UserCustomerRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\SetRepositoryInterface;
use App\Repositories\CustomerStoreRepositoryInterface;
use App\Repositories\LedgerRepositoryInterface;
use App\Repositories\SalePriceRepositoryInterface;
use App\Repositories\WarehouseAreaRepositoryInterface;
use App\Repositories\WarehouseShelfRepositoryInterface;
use App\Repositories\PurchasePriceRepositoryInterface;
use App\Repositories\Impl\AdminRepository;
use App\Repositories\Impl\BaseRepository;
use App\Repositories\Impl\BusinessPartnerRepository;
use App\Repositories\Impl\CategoryRepository;
use App\Repositories\Impl\CustomerDepartmentRepository;
use App\Repositories\Impl\CustomerRepository;
use App\Repositories\Impl\ProductRepository;
use App\Repositories\Impl\CustomerStoreRepository;
use App\Repositories\Impl\LedgerRepository;
use App\Repositories\Impl\UserRepository;
use App\Repositories\Impl\SetRepository;
use App\Repositories\Impl\SalePriceRepository;
use App\Repositories\Impl\WarehouseAreaRepository;
use App\Repositories\Impl\WarehouseShelfRepository;
use App\Repositories\Impl\PurchasePriceRepository;
use App\Services\AdminService;
use App\Services\BarCodeService;
use App\Services\BaseService;
use App\Services\BusinessPartnerService;
use App\Services\CategoryService;
use App\Services\BasicContractService;
use App\Services\ContractService;
use App\Services\ContractSetService;
use App\Services\CustomerDepartmentService;
use App\Services\CustomerService;
use App\Services\Impl\BarCodeServiceImpl;
use App\Services\Impl\LedgerListServiceImpl;
use App\Services\Impl\BasicContractServiceImpl;
use App\Services\Impl\ContractServiceImpl;
use App\Services\Impl\ContractSetServiceImpl;
use App\Services\Impl\ItemServiceImpl;
use App\Services\Impl\SetProductServiceImpl;
use App\Services\Impl\StoreAttributeServiceImpl;
use App\Services\ItemService;
use App\Services\CustomerStoreService;
use App\Services\LedgerService;
use App\Services\Impl\CustomerStoreServiceImpl;
use App\Services\Impl\LedgerServiceImpl;
use App\Services\SetProductService;
use App\Services\StoreAttributeService;
use App\Services\Impl\UserCustomerServiceImpl;
use App\Services\UserCustomerService;
use App\Services\UserService;
use App\Services\ProductService;
use App\Services\SetService;
use App\Services\LedgerListService;
use App\Services\SalePriceService;
use App\Services\WarehouseAreaService;
use App\Services\WarehouseShelfService;
use App\Services\PurchasePriceService;
use App\Services\Impl\AdminServiceImpl;
use App\Services\Impl\BaseServiceImpl;
use App\Services\Impl\BusinessPartnerServiceImpl;
use App\Services\Impl\CategoryServiceImpl;
use App\Services\Impl\CustomerDepartmentServiceImpl;
use App\Services\Impl\CustomerServiceImpl;
use App\Services\Impl\ProductServiceImpl;
use App\Services\Impl\UserServiceImpl;
use App\Services\Impl\SetServiceImpl;
use App\Services\Impl\SalePriceServiceImpl;
use App\Services\Impl\WarehouseAreaServiceImpl;
use App\Services\Impl\WarehouseShelfServiceImpl;
use App\Services\Impl\PurchasePriceServiceImpl;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindings($this->registerRepositories());
        $this->bindings($this->registerServices());
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindingMacros($this->registerMacros());
    }

    /**
     * Register services for binding
     *
     * @return string[]
     */
    private function registerServices(): array
    {
        return [
            BaseService::class => BaseServiceImpl::class,
            UserService::class => UserServiceImpl::class,
            AdminService::class => AdminServiceImpl::class,
            CategoryService::class => CategoryServiceImpl::class,
            CustomerService::class => CustomerServiceImpl::class,
            BusinessPartnerService::class => BusinessPartnerServiceImpl::class,
            ProductService::class => ProductServiceImpl::class,
            CustomerDepartmentService::class => CustomerDepartmentServiceImpl::class,
            SetService::class => SetServiceImpl::class,
            SalePriceService::class => SalePriceServiceImpl::class,
            ItemService::class => ItemServiceImpl::class,
            CustomerStoreService::class => CustomerStoreServiceImpl::class,
            LedgerService::class => LedgerServiceImpl::class,
            WarehouseShelfService::class => WarehouseShelfServiceImpl::class,
            StoreAttributeService::class => StoreAttributeServiceImpl::class,
            UserCustomerService::class => UserCustomerServiceImpl::class,
            BasicContractService::class => BasicContractServiceImpl::class,
            ContractService::class => ContractServiceImpl::class,
            ContractSetService::class => ContractSetServiceImpl::class,
            SetProductService::class => SetProductServiceImpl::class,
            PurchasePriceService::class => PurchasePriceServiceImpl::class,
            BarCodeService::class => BarCodeServiceImpl::class,
            LedgerListService::class => LedgerListServiceImpl::class,
        ];
    }

    /**
     * Register repositories for binding
     *
     * @return string[]
     */
    private function registerRepositories(): array
    {
        return [
            BaseRepositoryInterface::class => BaseRepository::class,
            UserRepositoryInterface::class => UserRepository::class,
            AdminRepositoryInterface::class => AdminRepository::class,
            CategoryRepositoryInterface::class => CategoryRepository::class,
            CustomerRepositoryInterface::class => CustomerRepository::class,
            BusinessPartnerRepositoryInterface::class => BusinessPartnerRepository::class,
            ProductRepositoryInterface::class => ProductRepository::class,
            CustomerDepartmentRepositoryInterface::class => CustomerDepartmentRepository::class,
            SetRepositoryInterface::class => SetRepository::class,
            SalePriceRepositoryInterface::class => SalePriceRepository::class,
            ItemRepositoryInterface::class => ItemRepository::class,
            CustomerStoreRepositoryInterface::class => CustomerStoreRepository::class,
            LedgerRepositoryInterface::class => LedgerRepository::class,
            WarehouseShelfRepositoryInterface::class => WarehouseShelfRepository::class,
            StoreAttributeRepositoryInterface::class => StoreAttributeRepository::class,
            UserCustomerRepositoryInterface::class => UserCustomerRepository::class,
            BasicContractRepositoryInterface::class => BasicContractRepository::class,
            ContractRepositoryInterface::class => ContractRepository::class,
            ContractSetRepositoryInterface::class => ContractSetRepository::class,
            SetProductRepositoryInterface::class => SetProductRepository::class,
            PurchasePriceRepositoryInterface::class => PurchasePriceRepository::class,
            BarCodeRepositoryInterface::class => BarCodeRepository::class,
        ];
    }

    /**
     * Loop all register to binding
     *
     * @param array $classes
     */
    private function bindings(array $classes)
    {
        foreach ($classes as $interface => $implement) {
            $this->app->bind($interface, $implement);
        }
    }

    private function registerMacros(): array
    {
        return [
            Str::class => [StrMixins::class]
        ];
    }

    /**
     * Binding macros' helper utils
     * @param array $classes
     */
    private function bindingMacros(array $classes)
    {
        foreach ($classes as $interface => $implements) {
            $interfaceClass = app($interface);
            foreach ($implements as $implementClass) {
                $interfaceClass::mixin(new $implementClass());
            }
        }
    }
}
