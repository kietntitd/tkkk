<?php

namespace App\Utils;

class MessageCommon {
    /**
     * Message create success
     */
    const MS02_001 = '登録が正常に完了しました。';

    /**
     * Message update success
     */
    const MS02_002 = '登録内容の変更が正常に完了しました。';

    /**
     * Message delete success
     */
    const MS02_003 = '正常に削除しました。';

    /**
     * Message create fail
     */
    const MS02_004 = 'データ登録が失敗しました。';

    /**
     * Message update fail
     */
    const MS02_005 = 'データ更新が失敗しました。';

    /**
     * Message delete fail
     */
    const MS02_006 = 'データ削除が失敗しました。';

    /**
     * Message check exclusive/concurrent
     */
    const MS02_011 = 'データが変更されたため、ページを再読み込んでください。';

    /**
     * Message check duplicate email
     */
    const MS03_0018 = 'このメールアドレスは既に別のアカウントで使用されています。';

    /**
     * Message URL not found
     */
    const MS01_001 = 'URLが正しくありません。';

    /**
     * Condition delete department
     */
    const MS03_043 = '台帳に従業員が存在している部署を削除できません。';

    /**
     * Message is not exist customer
     */
    const MS03_043_CUSTOMER = '指定した顧客は存在しないまたは無効になっています。';

    /**
     * Message is not exist product
     */
    const MS03_043_PRODUCT = '指定した商品は存在しないまたは無効になっています。';

    /**
     * API returns unknown error
     */
    const MS01_002 = 'エラーが発生しました。再度実行してください。';

    /**
     * Error authentication 401
     */
    const MS01_003 = '認証が失敗しました。';

    /**
     * Access denied
     */
    const MS01_004 = 'アクセス権限がありません。';

    /**
     * expired token
     */
    const MS01_005 = 'セッションタイムアウトが発生しました。';

    /**
     * Message is not exist business partner
     */
    const MS03_016_BUSINESS_PARTNER = '指定した取引先は存在しないまたは無効になっています。';
}


