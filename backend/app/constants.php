<?php
define('CODE_SUCCESS', 200);
define('CODE_CREATE_FAILED', 201);
define('CODE_DELETE_FAILED', 202);
define('CODE_MULTI_STATUS', 207);
define('CODE_NO_ACCESS', 403);
define('CODE_NOT_FOUND', 404);
define('CODE_ERROR_SERVER', 500);
define('CODE_UNAUTHORIZED', 401);

//Validate
define('V_VARCHAR_MAXLENGTH', '255');
define('V_VARCHAR_REQUIRED', 'required|max:' . V_VARCHAR_MAXLENGTH);
define('V_EMAIL_REQUIRED', 'required|email|max:' . V_VARCHAR_MAXLENGTH);
define('V_PASSWORD', 'required|max:20|min:8');
define('V_NAME_KANA', 'nullable|string|max:' . V_VARCHAR_MAXLENGTH);

//Date Time Format
define('DATE_FORMAT', 'Y-m-d');
